<?php

chdir(__DIR__);

require_once('../lib/Config.php');
require_once('../lib/QuickBooks.php');

$qb = new QuickBooks();
$qb->login();

$company = $qb->api('/v3/company/<realmID>/companyinfo/<realmID>');
echo $company->CompanyInfo->CompanyName . " Logged in";    