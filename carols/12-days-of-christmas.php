<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>12 Days of Christmas | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">The 12 Days of Christmas</h1>
								<div class="lyrics-body">
									<div id="lyrics-body-text" class="js-lyric-text">
									<p class="verse">On the first day of Christmas<br>
									my true love sent to me:<br>
									A Partridge in a Pear Tree</p><p class="verse">On the second day of Christmas<br>
									my true love sent to me:<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the third day of Christmas<br>
									my true love sent to me:<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the fourth day of Christmas<br>
									my true love sent to me:<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p>
									<p class="verse">On the fifth day of Christmas<br>
									my true love sent to me:<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the sixth day of Christmas<br>
									my true love sent to me:<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the seventh day of Christmas<br>
									my true love sent to me:<br>
									Seven Swans a Swimming<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the eighth day of Christmas<br>
									my true love sent to me:<br>
									Eight Maids a Milking<br>
									Seven Swans a Swimming<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the ninth day of Christmas<br>
									my true love sent to me:<br>
									Nine Ladies Dancing<br>
									Eight Maids a Milking<br>
									Seven Swans a Swimming<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the tenth day of Christmas<br>
									my true love sent to me:<br>
									Ten Lords a Leaping<br>
									Nine Ladies Dancing<br>
									Eight Maids a Milking<br>
									Seven Swans a Swimming<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the eleventh day of Christmas<br>
									my true love sent to me:<br>
									Eleven Pipers Piping<br>
									Ten Lords a Leaping<br>
									Nine Ladies Dancing<br>
									Eight Maids a Milking<br>
									Seven Swans a Swimming<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p><p class="verse">On the twelfth day of Christmas<br>
									my true love sent to me:<br>
									12 Drummers Drumming<br>
									Eleven Pipers Piping<br>
									Ten Lords a Leaping<br>
									Nine Ladies Dancing<br>
									Eight Maids a Milking<br>
									Seven Swans a Swimming<br>
									Six Geese a Laying<br>
									Five Golden Rings<br>
									Four Calling Birds<br>
									Three French Hens<br>
									Two Turtle Doves<br>
									and a Partridge in a Pear Tree</p>	</div>
									</div>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>