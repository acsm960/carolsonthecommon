<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Away in A Manger | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Away in A Manger</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<P>Away in a manger,
									<BR>No crib for a bed,
									<BR>The little lord Jesus
									<BR>Laid down his sweet head.
									<BR>The stars in the bright sky
									<BR>Looked down where he lay
									<BR>The little lord Jesus 
									<BR>Asleep on the hay.
									<BR>
									<BR>The cattle are lowing,
									<BR>The poor  baby awakes,
									<BR>But little lord Jesus,
									<BR>No crying he makes
									<BR>I love thee, lord Jesus
									<BR>Look down from the sky,
									<BR>And stay by my cradle
									<BR> Til morning is nigh
									<BR>
									<BR>Be near me, lord Jesus.
									<BR>I ask thee to stay
									<BR>Close by me forever,
									<BR>And love me, I pray.
									<BR>Bless all the dear children
									<BR>In thy tender care,
									<BR>And take us to heaven,
									<BR>To live with thee there
									<BR></P>
									
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>

