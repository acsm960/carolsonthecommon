<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Deck The Halls | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Deck the Halls</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>Deck the halls with boughs of Holly<br>Fa La La La La La La La La<br>Tis the season to be jolly<br>Fa La La La La La La La La<br>Done we now our gay apparel<br>Fa La La La La La La La La<br>Troll the Ancient Yuletide Carol<br>Fa La La La La La La La La<br><br>See the blazing Yule before us<br>Fa La La La La La La La La<br>Strike the harp and join the chorus<br>Fa La La La La La La La La<br>Follow me in merry measure<br>Fa La La La La La La La La<br>While I tell of Yule-tide treasure<br>Fa La La La La La La La<br><br>Fast away the old year passes<br>Fa La La La La La La La La<br>Hail the new, ye lads and lasses<br>Fa La La La La La La La La<br>Sing we joyous all together<br>Fa La La La La La La La La<br>Heedless of the wind and weather<br>Fa La La La La La La La La<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>