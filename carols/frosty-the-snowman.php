<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Frosty the Snowman | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Joyful Joyful</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>
										<br>Frosty the Snowman, 
										<br>was a jolly happy soul,
										<br>With a corn cob pipe 
										<br>and a button nose,
										<br> and two eyes made out of coal.
										<br> 
										<br>Frosty the Snowman,
										<br>is a fairytale, they say.
										<br>He was made of snow, 
										<br>but the children know 
										<br>how he came to life one day.
										<br>
										<br>There must have been 
										<br>some magic in that old silk 
										<br>hat they found,
										<br>For when they placed it on his head, he began to dance around!
										<br>Oh, Frosty, the Snowman,
										<br>was alive as he could be;
										<br>and the children say 
										<br>he could laugh and play,
										<br>just the same as 
										<br>you and me.
										<br>
										<br>
										<br>Frosty the Snowman, 
										<br>knew the sun was hot that day,
										<br>so he said, "Let's run,
										<br>and we'll have some fun, 
										<br>now, before I melt away."
										<br> 
										<br>Down to the village, 
										<br>with a broomstick in his hand,
										<br>Running here and there,
										<br>all around the square,
										<br>sayin', "Catch me if you can."
										<br>
										<br>
										He led them down the streets of town, right to the traffic cop;
										<br>and only paused a moment, when he heard him holler, "Stop!"
										<br>For Frosty, the Snowman,
										<br>had to hurry on his way,
										<br>But he waved goodbye, 
										<br>sayin' "Don't you cry, 
										<br>I'll be back again some day."

									</p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>