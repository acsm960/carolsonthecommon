<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Hark The Herald Angels Sing | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Hark the Herald Angels Sing</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>Hark the herald Angels sing<br>“Glory to the newborn King”<br>peace on earth and mercy mild<br>God and sinners reconciled”<br>Joyful, all you nations rise<br>join the triumph of the skies<br>with angelic hosts proclaim<br>“Christ is born in Bethlehem”<br>Hark the herald Angels sing<br>“Glory to the newborn King”<br><br>Christ by highest heaven adored<br>Christ the everlasting Lord<br>Late in time behold him come<br>Off spring of a Virgin’s womb<br>Veiled in flesh the God-Head see,<br>Hail Th’incarnate Deity<br>Pleased as man with man to dwell<br>Jesus our Emmanuel<br>Hark the Herald Angels sing<br>“Glory to the newborn King<br><br>Hail the Heaven born Prince of Peace!<br>Hail the sun of righteousness<br>Light and life to all he brings<br>Ris’n with healing in his wings<br>Mild he lays his glory by,<br>Born that we no more may die<br>Born to raise us from the earth<br>Born to give us second birth<br>Hark the Herald Angels sing<br>“Glory to the Newborn King.<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>