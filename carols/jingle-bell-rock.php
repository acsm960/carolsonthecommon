<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Jingle Bell Rock | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Jingle Bell Rock</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>
										Jingle bell, jingle bell, jingle bell rock<br>
																
										Jingle bells swing and jingle bells ring<br>
																
										Snowing and blowing up bushels of fun<br>
																
										Now the jingle hop has begun<br>
										<br>
																
										Jingle bell, jingle bell, jingle bell rock<br>
										Jingle bells chime in jingle bell time<br>
																
										Dancing and prancing in Jingle Bell Square<br>
																
										In the frosty air.<br>
										<br>
																
										What a bright time, it's the right time<br>
																
										To rock the night away<br>
																<br>
																
																
										Jingle bell time is a swell time<br>
																
										To go gliding in a one-horse sleigh<br>
																
										Giddy-up jingle horse, pick up your feet<br>
																
										Jingle around the clock<br>
																<br>
																
																
										Mix and a-mingle in the jingling feet<br>
																
										That's the jingle bell,<br>
																
										That's the jingle bell,<br>
																
										That's the jingle bell rock
										</p>									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>