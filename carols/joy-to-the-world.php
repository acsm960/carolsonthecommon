<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Joy to the World | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Joy to the World</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>Joy to the World , the Lord is come!<br>Let earth receive her King;<br>Let every heart prepare Him room,<br>And Heaven and nature sing,<br>And Heaven and nature sing,<br>And Heaven, and Heaven, and nature sing.<br><br>Joy to the World, the Savior reigns!<br>Let men their songs employ;<br>While fields and floods, rocks, hills and plains<br>Repeat the sounding joy,<br>Repeat the sounding joy,<br>Repeat, repeat, the sounding joy.<br><br>He rules the world with truth and grace,<br>And makes the nations prove<br>The glories of His righteousness,<br>And wonders of His love,<br>And wonders of His love,<br>And wonders, wonders, of His love.<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>