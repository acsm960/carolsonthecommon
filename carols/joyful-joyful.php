<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Joyful Joyful | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Joyful Joyful</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>
										<br>JOYFUL JOYFUL WE ADORE THEE
										<br>GOD OF GLORY, LORD OF LOVE
										<br>HEARTS UNFOLD LIKE FLOW’RS BEFORE THEE
										<br>OP’NING TO THE SUN ABOVE.
										<br>MELT THE CLOUDS OF SIN AND SADNESS;
										<br>DRIVE THE DARK OF DOUBT AWAY;
										<br>GIVER OF IMMORTAL GLADNESS.
										<br>FILL US WITH THE LIGHT OF DAY!
										<br><BR>
										<br>ALL THY WORKS WITH JOY SURROUND THEE
										<br>EARTH AND HEAV’N REFLECT THY RAYS
										<br>STARS AND ANGELS SING AROUND THEE,
										<br>CENTER OF UNBROKEN PRAISE.
										<br>FIELD AND FOREST, VALE AND MOUNTAIN,
										<br>FLOWRY MEADOW, FLASHING SEA,
										<br>SINGING BIRD AND FLOWING FOUNTAIN
										<br>CALL US TO REJOICE IN THEE
										<br><BR>
										<br>
										<br>THOU ART GIVING AND FORGIVING,
										<br>EVER BLESSING, EVER BLEST,
										<br>WELLSPRING OF THE JOY OF LIVING,
										<br>OCEAN DEPTH OF HAPPY REST!
										<br>THOU OUR FATHER, CHRIST OUR BROTHER,
										<br>ALL WHO LIVE IN LOVE ARE THINE;
										<br>TEACH US HOW TO LOVE EACH OTHER, 
										<br>LIFT US TO THE JOY DIVINE
										<br>
										<br><BR>
										<br>MORTALS, JOIN THE HAPPY CHORUS, 
										<br>WHICH THE MORNING STARS BEGAN;
										<br>FATHER LOVE IS REIGNING O’ER US,
										<br>BROTHER LOVE BINDS MAN TO MAN.
										<br>EVER SINGING, MARCH WE ONWARD,
										<br>VICTORS IN THE MIDST OF STRIFE,
										<br>JOYFUL MUSIC LEADS US SUNWARD
										<br>IN THE TRIUMPH SONG OF LIFE
									</p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>