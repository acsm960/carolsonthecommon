<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Little Drummer Boy | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Little Drummer Boy</h1>
								<p>Come they told me<br>Pa rum pum pum pum<br>A new born King to see<br>Pa rum pum pum pum<br>Our finest gifts we bring<br>Pa rum pum pum pum<br>To lay before the King<br>Pa rum pum pum pum<br>rum pum pum pum<br>so to honour him<br>Pa rum pum pum pum<br>When we come<br><br>Little baby<br>Pa rum pum pum pum<br>I am a poor boy too<br>Pa rum pum pum pum<br>I have no gift to bring<br>Pa rum pum pum pum<br>That’s fit to give our King<br>Pa rum pum pum pum<br>rum pum pum pum<br>rum pum pum pum<br>Shall I play for you<br>Pa rum pum pum pum<br>On my drum<br><br>Mary nodded<br>Pa rum pum pum pum<br>The ox and lamb kept time<br>Pa rum pum pum pum<br>I played my drum for Him<br>Pa rum pum pum pum<br>I played my best for Him<br>Pa rum pum pum pum<br>rum pum pum pum<br>rum pum pum pum<br>Then he smiled at me<br>Pa rum pum pum pum<br>me and my drum<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>