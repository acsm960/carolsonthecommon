<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Mary's Boy Child | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Mary's Boy Child</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>Long time ago in bethlehem<br>So the Holy Bible say<br>Mary’s Boy child, Jesus Christ<br>Was born on Christmas day<br><br>Hark now hear the angels sing<br>A new king born today<br>And man will live for evermore<br>Because of Christmas Day<br>Trumpets sound and angels sing<br>Listen to what they say<br>That man will live for evermore<br>Because of Christmas Day<br><br>While shepherds watched their flocks by night<br>Them see a bright a new shining star<br>Them hear a choir sing – <br>the music seemed to come from afar<br><br>Now Joseph and his wife, Mary<br>Come to Bethlehem that night<br>Them find no place to born her child<br>Not a single room was in sight.<br><br>Hark now hear the angels sing<br>A new king born today<br>And man will live for evermore<br>Because of Christmas Day<br>Trumpets sound and angels sing<br>Listen to what they say<br>That man will live for evermore<br>Because of Christmas Day<br><br>By and by they find a little nook<br>In a stable all forlorn<br>And in a manger cold and dark<br>Mary’s little boy was born<br>Long time ago in Bethlehem<br>So the Holy Bible say<br>Mary’s boy child Jesus Christ<br>Was born on Christmas Day<br><br>Hark now hear the angels sing<br>A new king born today<br>And man will live for evermore<br>Because of Christmas Day<br>Trumpets sound and angels sing<br>Listen to what they say<br>That man will live for evermore<br>Because of Christmas Day<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>