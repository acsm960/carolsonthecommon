<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>O Come All Ye Faithful | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">O Come All Ye Faithful</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>Oh, come, all ye faithful, <br>Joyful and triumphant!<br>Oh, come ye, oh, come ye to Bethlehem;<br>Come and behold him<br>Born the king of angels:<br>Oh, come, let us adore him, <br>Oh, come, let us adore him,<br>Oh, come, let us adore him,<br>Christ the Lord.<br><br>Sing, choirs of angels,<br>Sing in exultation,<br>Sing, all ye citizens of heaven above!<br>Glory to God<br>In the highest:<br>Oh, come, let us adore him, <br>Oh, come, let us adore him,<br>Oh, come, let us adore him,<br>Christ the Lord.<br><br>Yea, Lord, we greet thee,<br>Born this happy morning;<br>Jesus, to thee be glory given!<br>Word of the Father, <br>Now in flesh appearing!<br>Oh, come, let us adore him, <br>Oh, come, let us adore him,<br>Oh, come, let us adore him,<br>Christ the Lord.<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>

						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>