<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>O Holy Night | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">O Holy Night</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>O Holy night! The stars are brightly shining,<br>It is the night of the dear Saviour’s birth<br>Long lay the world in sing and error pining<br>Till he appeared, and the soul felt its worth.<br>A thrill of hope the weary soul rejoices<br>for yonder breaks a new and glorious morn.<br><br>Fall on your Knees, O hear the angel voices!<br>O night divine, O night when Christ was born!<br>O night , Divine, O night divine!<br><br>Led by the light of faith serenely beaming,<br>with glowing hearts by his cradle we stand,<br>so, led by light of a star sweetly gleaming,<br>here came the wise men from orient land.<br>The King of Kings lay thus in lowly manger<br>In all our trials born to be our friend,<br><br>Fall on your Knees, O hear the angel voices!<br>O night divine, O night when Christ was born!<br>O night , Divine, O night divine!<br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>