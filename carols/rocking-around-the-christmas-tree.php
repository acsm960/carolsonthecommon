<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Rockin' Around the Christmas Tree | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Rockin' Around the Christmas Tree</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<div class="lyrics-body">
										<div id="lyrics-body-text" class="js-lyric-text">
										<p class="verse">Rocking around the Christmas Tree<br>
										at the Christmas party hop<br>
										A mistletoe hung where you can see<br>
										Every couple tries to stop</p><p class="verse">Rocking around the Christmas Tree<br>
										Let the Christmas spirit ring<br>
										Later we'll have some pumpkin pie<br>
										and we'll do some caroling</p><p class="verse">You will get a sentimental feeling<br>
										When you hear, voices singing<br>
										"Let's be jolly; Deck the halls with boughs of holly"</p><p class="verse">Rocking around the Christmas Tree<br>
										Have a happy holiday<br>
										Everyone dancing merrily<br>
										In a new old fashioned way</p><p class="verse">You will get a sentimental feeling<br>
										When you hear voices singing<br>
										"Let's be jolly; Deck the halls with boughs of holly"</p><p class="verse">Rocking around the Christmas Tree<br>
										Have a happy holiday<br>
										Everyone's dancing merrily<br>
										In a new old Flashion way</p>	</div>
</div>						
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>