<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Rudolph The Red Nosed Reindeer | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Rudolph the Red Nosed Reindeer</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p>You know Dasher and Dancer and Prancer and Vixen,<br>you know Comet and Cupid and Donner and Blitzen,<br>But do you recall<br>The most famous reindeer of all<br><br>Rudolph the Red-Nosed Reindeer<br>Had a very shiny nose<br>And if you ever saw it<br>You would even say it glows<br>All of the other reindeer<br>Used to laugh and call him names<br>They never let poor Rudolph<br>Join in any reindeer games<br><br>Then one foggy Christmas Eve,<br>Santa came to say,<br>Rudolph with your nose so bright,<br>Won't you guide my sleigh tonight<br><br>Then how all the reindeer loved him,<br>As they shouted out with glee,<br>Rudolph the red-nose Reindeer<br>You'll go down in history<br><br>Rudolph the Red-Nosed Reindeer<br>Had a very shiny nose<br>And if you ever saw it,<br>You would even say it glows,<br>And all of the other reindeer<br>Used to laugh and call him names,<br>They never let poor Rudolph<br>Join in any reindeer games,<br><br>Then one foggy Christmas Eve,<br>Santa came to say,<br>Rudolph with your nose so bright,<br>Wont you guide my sleigh tonight<br><br>Then how all the reindeer loved him,<br>As they shouted out with glee,<br>Rudolph the Red-Nosed Reindeer,<br>You'll go down in history<br><br><br></p>
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>