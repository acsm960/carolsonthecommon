<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>We Three Kings | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">We Three Kings</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<P>
										We three kings of orient are
										<BR>Bearing gifts 
										<BR>We traverse afar.
										<BR>Field and fountain,
										<BR>Moor and mountain
										<BR>Following yonder star.
										<BR>
										<BR>O……
										<BR>Star of wonder, star of night.
										<BR>Star with royal
										<BR>Beauty bright.
										<BR>Westward leading,
										<BR>Still proceeding,
										<BR>Guide us to thy perfect light.
										<BR>Born a king on
										<BR>Bethlehem plain,
										<BR>Gold I bring to 
										<BR>Crown him again.
										<BR>King forever, ceasing never,
										<BR>Over us all to reign.
										<BR>
										<BR>O……
										<BR>Star of wonder, star of night.
										<BR>Star with royal
										<BR>Beauty bright.
										<BR>Westward leading,
										<BR>Still proceeding,
										<BR>Guide us to thy perfect light.
										<BR>
										<BR>
										<BR>Frankincense to offer have I
										<BR>Incense owns a deity nigh
										<BR>Prayer and praising, 
										<BR>All men raising
										<BR>Worship him, 
										<BR>God most high.
										<BR>
										<BR>O……
										<BR>Star of wonder, star of night.
										<BR>Star with royal
										<BR>Beauty bright.
										<BR>Westward leading,
										<BR>Still proceeding,
										<BR>Guide us to thy perfect light.
										<BR>
										<BR>Myrrh is mine 
										<BR>Its bitter perfume
										<BR>Breathes a life of 
										<BR>Gathering gloom
										<BR>Sorrowing, sighing, 
										<BR>Bleeding, dying,
										<BR>Sealed in the 
										<BR>Stone-cold tomb
										<BR>
										<BR>O……
										<BR>Star of wonder, star of night.
										<BR>Star with royal
										<BR>Beauty bright.
										<BR>Westward leading,
										<BR>Still proceeding,
										<BR>Guide us to thy perfect light.
										<BR>
										<BR>
										<BR>Glorious now 
										<BR>Behold him arise
										<BR>King and god 
										<BR>And sacrifice
										<BR>Alleluia, alleluia
										<BR>Earth to heaven replies
										<BR>
										<BR>O……
										<BR>Star of wonder, star of night.
										<BR>Star with royal
										<BR>Beauty bright.
										<BR>Westward leading,
										<BR>Still proceeding,
										<BR>Guide us to thy perfect light.
										<BR>
									</P>
									
									
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>

