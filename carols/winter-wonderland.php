<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Come and listen to all your favourite Christmas carols at Carols on the Common in North Ryde."/>
    
    <title>Winter Wounderland | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Winter Wonderland</h1>
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<P>
										Sleigh bells ring, 
										<BR>Are you listening,
										<BR>In the lane, 
										<BR>Snow is glistening
										<BR>A beautiful sight, 
										<BR>We're happy tonight,
										<BR>Walking in a winter
										<BR>Wonderland.
										<BR>
										<BR>Gone away is the bluebird,
										<BR>Here to stay is a new bird
										<BR>He sings a love song,
										<BR>As we go along,
										<BR>Walking in a winter wonderland
										<BR>
										<BR>In the meadow we can build a snowman
										<BR>Then pretend that he is parson brown
										<BR>He'll say are you married?
										<BR>We'll say no man
										<BR>But you can do the job
										<BR>When you're in town
										<BR>
										<BR>Later on
										<BR>We'll conspire
										<BR>As we dream by the fire
										<BR>To face unafraid
										<BR>The plans that we've made
										<BR>Walking in a winter wonderland
										<BR>
										<BR>In the meadow we can build a snowman
										<BR>And pretend that he's a circus clown
										<BR>We'll have lots of fun with mister snowman
										<BR>Until the other kiddies knock him down
										<BR>When it snows
										<BR>Ain't it thrilling
										<BR>Though your nose gets a chilling
										<BR>We'll frolic and play the eskimo way
										<BR>Walking in a winter wonderland
									</P>
									
									
									
									
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/songs-menu.php"); ?>
						
					</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>

