<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Donate to community groups to ensure their vital work continues.">
    
    <meta property="og:title" content="Donate | 15th Dec 2019"/>
    <meta property="og:description" content="Donate to the community to ensure their vital work continues."/>
    
    <title>Donate | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Donate to NSW Bushfire victims and fireys</h1>
								<!-- <p class="heading-line heading-line-lime super-lead">Can't remember a word or two? Don't worry - we've got you covered with the lyrics to all your favourite christmas carols.</p> -->
								<div class="col-sm-8 col-sm-offset-2 mobile-sidebar">
									<p class="lead">
										As part of our response to the NSW Bushfires, this year we will be dedicating a 7:30pm bucket collection to raise funds to support victims and firefighting crews.<BR><BR>

										We will be supported by volunteers from the Warringah headquarters Fire Brigade, who will bring their firefighting vehicle and participate in the bucket collection to raise awareness and funds. <BR><BR>

										Be sure to thank these brave volunteers and to give generously. 
									</p>
									<!-- <img src="images/romac/REX_KUMO.JPG" alt="Rex - a ROMAC patient" width="100%"><BR><BR>
									<p class="lead"><strong>
										There will be a collection on behalf of ROMAC during the Carols at 7:40 pm. Please give generously.<BR><BR> Alternatively, if you would like to make a donation online, click below to go directly to the secure online form at ROMAC's website.</strong></p>
									<hr>
									<a href="https://www.romac.org.au/donate" target="_blank" class=" btn btn-primary">Donate Now</a>
									<hr> -->
								</div>	
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>