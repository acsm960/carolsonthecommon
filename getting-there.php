<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Planning tips and how to get to the Rotary Carols on the Common Christmas event 2016 in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="15th Dec 2019 - Planning tips and how to get to the Rotary Carols on the Common Christmas event 2016 in North Ryde. Including fireworks, rides, food and carols."/>
    <title>Getting There & Planning | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once("inc/head.php"); ?>
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.css" type="text/css" rel="stylesheet" />
    </head><!--/head-->

<body class="volunteers page">
	<?php include_once("inc/header.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center">
						<h1 class="large mb16 mb-xs-24">Getting there & Planning</h1>
							<p class="heading-line heading-line-lime super-lead">Join us for an authentic carols experience at Sydney's second largest Christmas carols.</p>
							<p class="lead">Rotary Carols on the Common is held at the North Ryde Common (Cnr Twin Rd and Wicks Rd, North Ryde). Parking is available on site (for a gold-coin donation) and in surrounding streets. Amenities such as food, drinks and bathrooms are all located on-site. </p>
					</div>
				</div>
			</div>
		</section>
		
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-md-push-7">
						<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:500px;width:100%;"><div id="gmap_canvas" style="height:500px;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.themecircle.net" id="get-map-data">website</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:15,center:new google.maps.LatLng(-33.8015959,151.1224793),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(-33.8015959, 151.1224793)});infowindow = new google.maps.InfoWindow({content:"<b>Carols on the Common</b><br/>Ryde Common, North Ryde<br/>2113 Sydney" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>	
					<!-- Start Transport Info iframe -->
						<iframe class="center-block" style="float: none; margin-left: auto; margin-right: auto;" src='//www.transport.nsw.gov.au/sites/all/modules/transport_131500/widgets/Landscape131500.html' width='440' height='216' 
						  marginwidth='0' marginheight='0' frameborder='0' scrolling='no' 
						  title='Transport Info trip planner.'>
						  <p>If you can see this text, your web browser does not support iframes. 
						    This iframe contains the Transport Info trip planner. If you wish to use the 
						    Trip Planner, please visit the <a href='http://www.transportnsw.info'> 
						    Transport Info website</a>.</p>
						</iframe>
						<!-- End Transport Info iframe -->
	
					</div>
					<div class="col-md-7 col-md-pull-5">
					<!-- <a href="#" data-featherlight="images/poster/2016/weather.png"><BR>
					<img src="images/poster/2016/weather.png" alt="Carols on the Common Weather relocation plan"  width="100%">View Larger</a>
					<P>WET WEATHER UPDATE: The forecast is looking great for Sunday and the carols will go ahead as planned. The precise location of the carols will be slightly shifted away from Wicks Rd and into the Macquarie Hospital carpark opposite Nerang St.</P>

					<P>All your favourite rides, games, food stalls, activities and entertainment will proceed as usual. The preshow begins at 5:30pm and the carols commence at 8pm followed by an awesome fireworks display at 9:15pm. We can’t wait to see you there. 
					</P> -->
					<h2 class="color-orange  text-center">Planning</h2>
					<P><strong>Food is available on-site :</strong>Food will be sold by a range of food vendors on the night. It's best to bring cash, as cards cannot always be accepted.</P>
					<P><strong>Drinks/Alcohol :</strong> Soft drinks will be available at vendors but please bring a water bottle to refill at a water station, as there will be no bottled water available on the night in an effort to minimise plastic wastage. BYO alcohol.</P>
					<P><strong>Rubbish and Recycling :</strong> There will be minimal bins on the night so we kindly ask that you take all rubbish home with you at the end of the night.</P>
					<P><strong>Toilet facilities :</strong>Public toilets are available on the night. </P>
					<P><strong>Seating :</strong>Be sure to pack a picnic rug and/or chairs. The Common slopes towards the stage, so there is plenty of space with great views from all angles.</P>
					<P>Don't forget the following things.</P>
					<ul>
						<li>A hat, sunscreen and water</li>
						<li>A picnic rug and/or chairs</li>
						<li>BYO alcohol</li>
						<li>Cash for food, drinks and rides</li>
					</ul>
					
					<h2 class="color-green  text-center">Parking</h2>
					<P>Parking is available on-site for a gold-coin donation. Entry to parking is via Wicks Rd. This usually fills by 6 pm, so be sure to arrive early. Additional parking is available in the surrounding streets.</P>
					
					
					<h2 class="color-teal  text-center">Driving</h2>
					<h3>From the City</h3>
					<P>Take M1 and Lane Cove Tunnel to Epping Rd in North Ryde. Take the exit for EPPING Rd from The Lane Cove Tunnel. After passing Domayne North Ryde, take the next left onto Wicks Rd. The North Ryde Common is 1.2 km on the left.</P>
					<h3>From the Chatswood / Pacific Hwy</h3>
					<P>Take FULLERS Rd, near Chatswood Toyota. Continue as FULLERS Rd becomes DELHI Rd. Join EPPING Rd turning right. Take the next left onto Wicks Rd. The North Ryde Common is 1.2 km on the left.</P>
					<h3>From Ryde / Victoria Rd</h3>
					<P>Take LANE COVE Rd Northbound. Take the GOUDLING Rd turn off to the right. Follow the two curves in the road and the North Ryde Common is on the right.</P>


					<h2 class="color-yellow  text-center">Public Transport</h2>
					
					<P>The North Ryde Common is a 27 minute walk from North Ryde Train station. </P>
					<P>North Ryde Common is serviced by the following bus routes.</P>
					<P><strong>Exit at Cox's Rd Bus Stop, walk to Wicks Rd </strong>: 286, 287, 288, 297, 506</P>
					<P><strong>Exit at Wicks Rd Bus Stop</strong>: 533, 534</P>
					<P>From the City use 288, 506</P>
					<P>From Macquarie Centre use 288</P>
					<P>From Epping Station use 288</P>
					<P>From Chatswood use 534</P>
					
											
				</div>
				</div>
			</div>
		</section>
	</div>
	<hr> 
	<?php include_once("inc/footer.php"); ?>
    <script src="//code.jquery.com/jquery-latest.js"></script>
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>
