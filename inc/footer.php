<footer id="footer">
        <div class="container">
            <div class="text-center">
                <!-- Begin MailChimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                  #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
                  /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                     We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                </style>
                <div id="mc_embed_signup">
                <form action="//carolsonthecommon.us14.list-manage.com/subscribe/post?u=c8500017cdb4332fd1b4034c1&amp;id=a018f887df" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                  <label for="mce-EMAIL">Be the first to know about next year's carols... </label>
                  <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c8500017cdb4332fd1b4034c1_a018f887df" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>

                <!--End mc_embed_signup-->
                 <p> Copyright  &copy;2018. All Rights Reserved. <br> Website by <a target="_blank" href="http://acsmallhorn.com/websites/">Adam</a>. <BR>Hosting donated by <a href="http://www.completewebservices.com.au" target="_blank">Complete Web Services</a></p>               
            </div>
        </div>
    </footer>
    <!--/#footer-->
  
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true&key=AIzaSyDGABnKZKxNa9Bpv_m7TUFDLztUNJrelzA"></script>
  	<script type="text/javascript" src="/js/gmaps.js"></script>
	<script type="text/javascript" src="/js/smoothscroll.js"></script>
    <script type="text/javascript" src="/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="/js/coundown-timer.js"></script>
    <script type="text/javascript" src="/js/jquery.scrollTo.js"></script>
<!--     <script type="text/javascript" src="js/jquery.nav.js"></script> -->
    <script type="text/javascript" src="/js/main.js"></script>  
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70707961-1', 'auto');
  ga('send', 'pageview');

</script>
