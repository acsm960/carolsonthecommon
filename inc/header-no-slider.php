<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				<div class="header-top">
					<div class="pull-right social-icons">
						<!--a href="#"><i class="fa fa-twitter"></i></a-->
						<a href="https://www.facebook.com/RotaryCarolsOnTheCommon/"><i class="fa fa-facebook"></i></a>
						<!--a href="#"><i class="fa fa-google-plus"></i></a-->
						<!--a href="#"><i class="fa fa-youtube"></i></a-->
					</div>
				</div>     
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="/">
		                	<img class="img-responsive" src="/images/logo.png" alt="logo">
		                </a>                    
		            </div>
		            <div class="collapse navbar-collapse">
		                <ul class="nav navbar-nav navbar-right">                 
		                    <li class="scroll"><a href="/">Home</a></li> 
		                    <li><a href="http://www.carolsonthecommon.org.au/schedule">Schedule</a></li>                       
		                    <li class="scroll"><a href="/getting-there/">Getting there</a></li>
		                     <li class="scroll"><a href="/romac/">ROMAC Charity</a></li>
		                    <li class="scroll"><a href="/volunteers/">Volunteers</a></li> 
		                    <li class="scroll"><a href="/sponsors/">Sponsors</a></li>                       
		                         
		                </ul>
		            </div>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 
    
    <section class="no-slider" id="home">	
    </section>
	<!-- <?php include_once("slider.php"); ?> -->
    