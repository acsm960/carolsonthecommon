
<? 

	$values = SponsorShip::getInvoice($_GET); 

	$values = SponsorShip::updateInvoice($_POST, 'invoice?ID={ID}&thanks=1'); 
	if ($values==null) {
			$values = new RowClass();
	}

?>

<style>
	label { display:block; float:left; width:150px; text-align:right; padding-right:10px; font-weight:bold }
	.errorMessage { font-weight:bold; color:white; padding-left:160px }
	table.centertd, input.text, textarea.text { width: 400px }
	.centertd td { text-align:center;  }
	.centertd th { text-align:center; width:80px; }
	.line { margin-bottom:15px }
<? if (count($values->_messages)>0): ?>
	.contentbg { height:880px }
<? else: ?>
	.contentbg { height:820px }
<? endif ?>
		
</style>
<h2>Sponsorship Form</h2>

<table cellpadding="0" cellspacing="0" border="0"><tr>
<td>
<? ob_start() ?>
<!--DATASTART <?=$values->export()?> -->
<form onsubmit="$('#SubmitForm').css('opacity', 0.4); $('#SubmitForm').val('Processing ...'); $('#SubmitForm').attr('disabled','yes'); "  id="invoice" method="post" style="width:49%">
<input type="hidden" name="ID" value="Add" />
<div class="line">
	<label>Contact Email*:</label>
    <input required="yes" class="text" type="text" name="Email" style="width:38%" /> 
</div>
<div class="line">
	<label>Contact Name*:</label>
  <input  required="yes"  class="text" placeholder="Given Name" onchange="checkExists()" style="width:15%" type="text" name="GivenName" />
  <input  required="yes" class="text" placeholder="Family Name" onchange="checkExists()"  style="width:55%"  type="text" name="FamilyName" />
</div>
<div class="line">
	<label>Contact phone*:</label>
  <input required="yes"  class="text" type="text" name="PrimaryPhone" />
</div>
<div class="line">
	<label>Company Name*:</label>
    <input  required="yes"  class="text" type="text" name="CompanyName" />
</div>
<div class="line">
	<label style="height: 100px">Address (optional):</label>
    <input class="text" type="text" name="Line1" 
    /><input class="text" type="text" style="width:35%" name="City" placeholder="City/Suburb" 
    /><input class="text" type="text" style="width:35%"  name="CountrySubDivisionCode" placeholder="State" 
    /><input class="text" type="text"  style="width:35%" name="PostalCode" placeholder="Post Code" 
    /><input class="text" type="text"  style="width:35%" name="Country" placeholder="Country" value="Australia" />
    
</div>


<div class="line">
	<label>Sonsorship level*:</label>
    <select  required="yes"  onchange="$('#SponsorshipAmount').val(this.options[this.selectedIndex].getAttribute('amount'));" name="Sponsorship">
        <option value="">Select ...</option>
        <? setlocale(LC_MONETARY, 'en_AU'); foreach (SponsorShip::$Amounts as $name => $amount): ?>
        <option amount="<?=$amount?>" value="<?=$name?>"><?=money_format('%n',$amount)?> <?=$name?></option>
        <? endforeach ?>
    </select>
    <input type="hidden" id="SponsorshipAmount" name="Amount">
</div>

<div class="line" style="clear:both">
	<label>Comments:</label>
        <table cellpadding="0" cellspacing="0" border="0"><tr><td>
		    <textarea name="Notes" rows="5" class="text"></textarea>
        </td></tr></table>
</div>
<div class="line">
	<label>&nbsp;</label>
    <input id="SubmitForm" class="text"  type="submit" value="Click Here to Complete Request" />
    <input type="hidden" name="_save" value="Save />
</div>
</form>
<?=formMagic() ?>

