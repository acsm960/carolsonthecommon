<?
$isAdmin = $_SERVER['REMOTE_USER']=='pwillia6';
if ($isAdmin) {
    $_SESSION['ROSTER'] = 1;
}

if (isset($_GET['send'])) {
    $isAdmin = false;
    $_SESSION['Registrations'] = array();
}

$r = Registration::getRoster();

?>
<!--
<style type="text/css">
  * { font-family: Arial, sans-serif }
  table td, table th { text-align: left; vertical-align:top; border: 1px solid #000 }
  table { border-collapse: collapse }
  table th, table td { padding: 4px };
</style>
-->

<? $keys = array ( 'Day 11:00-2:00pm',   'Evening 5:30-9:00pm', 'Parking Attendant', 'Pull Down from 9:30pm') ?>
<p>Hi Everyone,</p>
<p><b>Thank you</b> for volunteering to help out with the Carols.</p>
<p><b>Important</b>  this roster has been updated mainly because of alignment problems.   There are a few changes.</p>
<p>As per before we are a little short on numbers so pleae let me know if you can't turn up.</p>
<p>Please be prepared to do other jobs you are not rostered for.   In particular we are short of servers from 7:30pm.</p>
<!--<p><b>You can change </b> you selection by going to the <a href="registration.php">registration page<a> and entering your name and email.  This will influence where you are on the roster.</p> -->
<table border="1" style="border-collapse: collapse">
<? foreach ($keys as $major):
      $minors = $r['Roster'][$major];
?>
  <tr>
      <th style="text-align: left; vertical-align:top; border: 1px solid #000; padding: 4px" valign="top" rowspan="<?=count($minors)?>"><?=str_replace("11:00","10:30", $major)?></th>
      <? foreach ($minors as $minor => $people ): ?>
        <th valign="top" style="text-align: left; vertical-align:top; border: 1px solid #000; padding: 4px"><?=$minor?></th>
        <td valign="top" style="text-align: left; vertical-align:top; border: 1px solid #000; padding: 4px">
            <? foreach ($people as $person): ?>
            <div class="person">
              <? if ($isAdmin || isset($_SESSION['Registrations'][$person->ID])): ?>
                <? if (isset($_GET['email'])): ?>
                <?=$person->Name?> &lt;<?=$person->Email?>&gt;
                <? elseif (isset($_GET['plain'])): ?>
                <?=$person->Name?> (<?=$person->GroupClub?>);
                <? else: ?>
                <a href="registration.php?ID=<?=$person->ID?>" target="_blank"><?=$person->Name?> (<?=$person->GroupClub?>)</a>
                <? endif ?>
              <? else: ?>
                <?=$person->Name?> (<?=$person->GroupClub?>)
              <? endif ?>
            </div>
            
            <? endforeach?>
          
          
        </td>
        </tr>
      <? endforeach ?>
    
    
  </tr>
<? endforeach ?>
</table>
<? if (!isset($_GET['send'])): ?>
<h1>Unallocated</h1>
<p>Please do not be upset if your name is here.  It probably means you are very flexible or there are other circumstances.  You will be given something to do.</p>
<table>
  <tr><th>Name</th><th>Comment</th><th>Activities</th></tj></tr>
  <? foreach($r['Unallocated'] as $row): ?>
  <tr>
    <td>
        <? if ($isAdmin || isset($_SESSION['Registrations'][$row->ID])): ?>
          <a href="registration.php?ID=<?=$row->ID?>" target="_blank"><?=$row->Name?> (<?=$row->GroupClub?>)</a>
        <? else: ?>
          <?=$row->h_Name?> (<?=$row->GroupClub?>)
        <? endif ?>
    
    </td>
    <td width="40%"><?=$row->h_Comments?></td>
    <td><?=str_replace('|', ' ', str_replace(',','<br>',$row->h_Shift))?></td>
  </tr>
  <? endforeach ?>
</table>
<? endif ?>

