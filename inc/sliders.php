<section class="slider" id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
				<li data-target="#main-slider" data-slide-to="3"></li>
				<li data-target="#main-slider" data-slide-to="4"></li>
				<li data-target="#main-slider" data-slide-to="5"></li>
				<li data-target="#main-slider" data-slide-to="6"></li>
				<li data-target="#main-slider" data-slide-to="7"></li>
				<li data-target="#main-slider" data-slide-to="8"></li>
				<li data-target="#main-slider" data-slide-to="9"></li>

			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img class="img-responsive" src="/images/slider/bg3.jpg" alt="slider">						
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>


				<div class="item">
					<img class="img-responsive" src="/images/slider/2016carols3.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>

				<div class="item">
					<img class="img-responsive" src="/images/slider/2016carols10.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="/images/slider/2016carols8.JPG" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="/images/slider/2016carols1.JPG" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>





				<div class="item">
					<img class="img-responsive" src="/images/slider/bg2.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="/images/slider/bg1.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="/images/slider/2016carols4.JPG" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>	
				<div class="item">
					<img class="img-responsive" src="/images/slider/bg5.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="/images/slider/2016carols7.JPG" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<!-- <div class="item">
					<img class="img-responsive" src="/images/slider/bg7.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>Carols on the Common </h2>
						<h3>Carols - Fireworks - Rides - Food trucks</h3>
						<h4>16th December 2018</h4>
						<a href="#contact">MORE DETAILS <i class="fa fa-angle-right"></i></a>
					</div>
				</div>		 -->		
			</div>
		</div>    	
    </section>