						<hr>
						<div class="carols-menu">
							<a href="/carols/o-come-all-ye-faithful" class=" btn btn-primary btn-full-width">O Come All Ye Faithful</a>
							<a href="/carols/the-first-noel" class=" btn btn-primary btn-full-width">The First Noel</a>
							<a href="/carols/away-in-a-manger" class=" btn btn-primary btn-full-width">Away in A Manger</a>
							<a href="/carols/joy-to-the-world" class=" btn btn-primary btn-full-width">Joy to the World</a>
							<a href="/carols/o-holy-night" class=" btn btn-primary btn-full-width">O Holy Night</a>
							<a href="/carols/rudolph-the-red-nosed-reindeer" class=" btn btn-primary btn-full-width">Rudolph the Red Nosed Reindeer</a>
							<a href="/carols/winter-wonderland" class=" btn btn-primary btn-full-width">Winter Wonderland</a>
							<a href="/carols/angels-we-have-heard-on-high" class=" btn btn-primary btn-full-width">Angels We Have Heard on High</a>
							<a href="/carols/we-three-kings" class=" btn btn-primary btn-full-width">We Three Kings</a>
							<a href="/carols/joyful-joyful" class=" btn btn-primary btn-full-width">Joyful Joyful</a>
							<a href="/carols/marys-boy-child" class=" btn btn-primary btn-full-width">Mary's Boy Child</a>
							<a href="/carols/jingle-bell-rock" class=" btn btn-primary btn-full-width">Jingle Bell Rock</a>
							<a href="/carols/rocking-around-the-christmas-tree" class=" btn btn-primary btn-full-width">Rockin' Around The Christmas Tree</a>
							<a href="/carols/let-there-be-peace-on-earth" class=" btn btn-primary btn-full-width">Let There Be Peace On Earth</a>
							<a href="/carols/deck-the-halls" class=" btn btn-primary btn-full-width">Deck the Halls</a>
							<a href="/carols/hark-the-herald-angels-sing" class=" btn btn-primary btn-full-width">Hark the Herald Angels Sing</a>
							<a href="/carols/little-drummer-boy" class=" btn btn-primary btn-full-width">Little Drummer Boy</a>
							<a href="/carols/frosty-the-snowman" class=" btn btn-primary btn-full-width">Frosty the Snowman</a>
							<a href="/carols/santa-claus-is-coming-to-town" class=" btn btn-primary btn-full-width">Santa Claus is Coming to Town</a>
							
							<a href="/carols/silent-night" class=" btn btn-primary btn-full-width">Silent Night</a>
							<a href="/carols/feliz-navidad" class=" btn btn-primary btn-full-width">Feliz Navidad</a>
							<!-- <a href="/carols/12-days-of-christmas" class=" btn btn-primary btn-full-width">12 Days of Christmas</a> -->
						</div>
						<hr>