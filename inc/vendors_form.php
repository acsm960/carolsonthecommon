<? $update = true ?>		

<? 
	$values = Vendor::updateVendor($_POST, 'vendors?ID={ID}&thanks=1'); 
	if ($values==null) {
		if (isset($_GET['ID']))
			$values = Vendor::getVendor($_GET['ID']);
	}
	if ($values==null) {
			$values = new RowClass();
	}

?>

<script type="text/javascript">
	AJAXJsonCounter = 0;
	AJAXScriptElement = false;
	function getJSON(url, loader) {
		if (AJAXScriptElement)
					AJAXScriptElement.parentNode.removeNode(AJAXScriptElement);
			var AJAXScriptElement = document.createElement('script');
			document.body.appendChild(AJAXScriptElement);
			AJAXScriptElement.src = url + '&loader=' + loader + '&rand=' + String(AJAXJsonCounter++);
			return;
	}
	
	function loadValues(data) {
		var form = document.getElementById('registration');
		for (var name in data) {
			var value = data[name];

			var el = form[name];
			if (el==null) {
				els = form[name + '[]'];
				if (els!=null) {
					for (var ei=0; ei<els.length; ei++) { 
					    var el = els.item(ei);
						for (var vi in value) {
								var v = value[vi];
								if (el.value==v) {
									el.checked = true;
								}
							}
						}
					}
					continue;
				}
			if (el==null) continue;
			if (el.tagName=='INPUT') {
				if (el.type=='hidden' || el.type=='text'  || el.type=='email'  || el.type=='tel'  || el.type=='number')
					el.value = value;
				if (el.type=='checkbox' &&  value != '') {
						el.checked = true;
				}
			}
			if (el.tagName=='TEXTAREA') {
				el.value = value;
			}
			if (el.tagName=='SELECT') {
				for (var oi=0; oi < el.options.length; oi++) {
					var oel = el.options.item(oi);
					if (oel.value==value) {
						oel.selected = true;
					}
				}
			}
		}
	}
	
	function checkExists() {
		var form = document.getElementById('registration');
		var email = form.Email.value;
		var name  = form.ContactName.value;
		if (email!='' && name!='')
			  getJSON('search_vendor.php?email=' + escape(email) + '&name=' + escape(name), 'loadValues');
	}
  
  /* This page has all the scripts loaded at the bottom */
  $jqwait = window.setInterval(function(){
      if (typeof($)=='undefined') {
          return;
      }
      window.clearInterval($jqwait);
    
      $(document).ready(function(){
          var required = [];
          $('.line [name]').parent().find('label:contains("*")').each(function(){
               required.push($(this).parent().find('[name]').attr('name'));
               $(this).parent().find('[name]').attr('required', 'yes');
          });
          $('[name="_required"]').val(required.toString());
        
        
      });
  }, 100);
</script>

<style>
  #mc_embed_signup { display: none }
	.hint { padding-left: 150px; margin-top:-2px; padding-bottom:10px}
  label { display:block; float:left; width:150px; text-align:right; padding-right:10px; font-weight:bold }
	.errorMessage { font-weight:bold;  padding-left:150px; color: #F26A67; }
	table.centertd, input.text, textarea.text { width: 400px }
	.centertd td { text-align:center;  }
	.centertd th { text-align:center; width:80px; }
	.line { margin-bottom:15px }
<? if (count($values->_messages)>0): ?>
	.contentbg { height:880px }
<? else: ?>
	.contentbg { height:820px }
<? endif ?>
		
</style>
<h2>Application Form</h2>

<table cellpadding="0" cellspacing="0" border="0"><tr>
<td>
<? if (!$update): ?>
<p>Applications are now closed</p>
<p>If you wish to update your details please call Paul Williamson on SMS 0425 859 876 (please do not email me).</p>
<p>Thanks, <br />Paul</p>
<? endif ?>
<? if ($update): ?>
<? ob_start() ?>
<!--DATASTART <?=$values->export()?> -->
<form id="registration" method="post" style="width:49%">
<input type="hidden" name="ID" value="Add" />
<input type="hidden" name="_required" value="" />
<div class="line">
	<label>Contact Name*:</label>
    <input class="text type="text" onchange="checkExists()"  name="ContactName" />
    <div style="padding:0" class="hint">Note: Fill in name and email to modify existing application</div>
</div>

<div class="line">
	<label>Email contact*:</label>
    <input class="text" onchange="checkExists()" type="email" name="Email" />
</div>

<div class="line">
	<label>Stall Name*:</label>
    <input class="text" type="text" name="Name" />
</div>

<div class="line">
	<label>ABN*:</label>
    <input class="text" type="text" name="ABN" />
</div>

<div class="line">
	<label>Postal Address*:</label>
    <textarea class="text" type="text" name="PostalAddress"></textarea>
</div>

<div class="line">
	<label>Food Preparation Address*:</label>
    <textarea class="text" type="text" name="PreparationAddress"></textarea>
    <div class="hint">Enter 'NA' here if not applicable</div>
</div>



<div class="line">
	<label>Mobile phone*:</label>
    <input class="text" type="tel" name="MobilePhone" />
</div>
<div class="line">
	<label>Office phone:</label>
    <input class="text" type="text" name="OfficePhone" />
    <div class="hint">(optional)</div>
</div>

<div class="line">
	<label>Description*:</label>
    <div><textarea class="text" name="Description"></textarea></div>
    <div class="hint">Please describe the goods and/or services your will be providing at your stall.   Please ensure this description is complete.<br>Note: Cold drinks can not be sold.</div>
</div>

<div class="line">
	<label>Comments:</label>
    <div><textarea class="text" name="Comments"></textarea></div>
    <div class="hint">Anthing else you want to tell us</div>
</div>


<div class="line">
	<label>Stall width*:</label>
    <input class="text" style="width:40px" type="number" name="Width" /> m
</div>


<div class="line">
	<label>Power requirement:</label>
    <!-- <div><textarea class="text" name="PowerRequirement"></textarea></div>-->
    <div>(Power is not being supplied due to complications)</div>
    <!--<div class="hint">(optional) Limited power is available on site.   We need to know what you are powering and how much current (in amps) it will draw.  Please describe what powere requirements you may have and if your own your own generator.</div>-->
</div>

<div class="line
">
	<label>Public liability insurance*:</label>
    <input type="checkbox" value="Yes" name="PublicLiability">
    <div class="hint">By ticking this box you undestand thay you will be required to furnish us with a currency certificate for the date of the carols.</div>
</div>

<div class="line">
	<label>&nbsp;</label>
    <input class="text"  type="submit" name="_save" value="Click Here to Process Application" />
</div>

</form>
<?=formMagic() ?>
<? endif ?>

