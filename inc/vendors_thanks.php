<? 
	$values = Vendor::getVendor($_GET['ID']);
?>
<!--DATASTART <?=$values->export()?> -->
<style>
	label { display:block; float:left; width:120px; text-align:right; padding-right:10px; font-weight:bold }
	table.centertd, input.text, textarea.text { width: 400px }
	.centertd td { text-align:center; border: 1px solid #446F97; font-size:12px; }
	.centertd th { text-align:center; width:80px; font-size:13px }
	.line { margin-bottom:5px; margin-top:8px; clear: both; border-bottom:1px solid #ccc; width:95% }
</style>
<h2>Vendor Registration Details</h2>
<p>Please check your details:</p>
<p><input type="button" onclick="location.href='vendors?ID=<?=$_GET['ID']?>'" style="width:95%" value="Please click here to correct if required" /></p>
<div class="line">	<label>ID:</label>    <?=$values->h_ID?></div>
<div class="line">	<label>Stall Name:</label>    <?=$values->h_Name?></div>
<div class="line">	<label>ABN:</label>    <?=$values->h_ABN?></div>
<div class="line">	<label>Postal Address:</label>    <?=$values->PostalAddress?></div>
<div class="line">	<label>Preparation Address:</label>    <?=$values->PreparationAddress?></div>
<div class="line">	<label>Contact:</label>    <?=$values->h_ContactName?></div>
<div class="line">	<label>Email:</label>    <?=$values->e_Email?></div>
<div class="line">	<label>Mobile Phone:</label>    <?=$values->h_MobilePhone?></div>
<div class="line">	<label>Office Phone:</label>    <?=$values->h_OfficePhone?></div>
<div class="line">	<label>Description:</label>    <?=$values->h_Description?></div>
<div class="line">	<label>Comments:</label>    <?=$values->h_Comments?></div>
<div class="line">	<label>Width:</label>    <?=$values->h_Width?></div>
<div class="line">	<label>Power requirement:</label>    <?=$values->h_PowerRequirement?></div>
<div class="line">	<label>Description:</label>    <?=$values->h_Description?></div>
<div class="line">	<label>Public Liability:</label>    <?=$values->h_PublicLiability?></div>
<p><input type="button" onclick="location.href='vendors?ID=<?=$_GET['ID']?>'"  style="width:95%" value="Please click here to correct if required" /></p>
<p>Note: You can also change or check your registration by re-entering your name and email address (exactly the same) at a later date on the registration form.
