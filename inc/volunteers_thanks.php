<? 
	$values = Registration::getRegistration($_GET['ID']); 
?>
<!--DATASTART <?=$values->export()?> -->
<style>
	label { display:block; float:left; width:120px; text-align:right; padding-right:10px; font-weight:bold }
	table.centertd, input.text, textarea.text { width: 400px }
	.centertd td { text-align:center; border: 1px solid #446F97; font-size:12px; }
	.centertd th { text-align:center; width:80px; font-size:13px }
	.line { margin-bottom:15px }
</style>
<h2>Volunteer Registration Details</h2>
<p>Please check your details:</p>
<p><input type="button" onclick="location.href='volunteers?ID=<?=$_GET['ID']?>'" style="width:95%" value="Please click here to correct if required" /></p>
<div class="line">
	<label>Name:</label>
    <?=$values->h_Name?>
</div>
<div class="line">
	<label>Email:</label>
    <?=$values->h_Email?>
</div>
<div class="line">
	<label>Group/Club:</label>
    <?=$values->h_GroupClub?>
</div>
<!--
<div class="line">
	<label>Extra people:</label>
    <?=$values->ExtraPeople=='' ? '0' : $values->ExtraPeople?>
</div>
-->
<div class="line">
	<label>Available:</label>
    <table cellpadding="0" cellspacing="0"><tr><td>
		<?=str_replace('|' , ' - ', str_replace(',', '<br>', implode(',', $values->Shift)))?>
    </td></tr></table>
</div>
<div class="line" style="clear:both">
	<label>Comments:</label>
    <table cellpadding="0" cellspacing="0" border="0"><tr><td><?=$values->tx_Comments?></td></tr></table>
</div>
<div class="line">
	<label>Mailing List:</label>
    <?=$values->MailingList=='Yes' ? 'Yes' : 'No' ?>
</div>
<p><input type="button" onclick="location.href='volunteers?ID=<?=$_GET['ID']?>'"  style="width:95%" value="Please click here to correct if required" /></p>
<p>Note: You can also change or check your registration by re-entering your name and email address (exactly the same) at a later date on the registration form.
