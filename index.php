<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Join us for an authentic carols experience at Sydney's second largest Christmas carols in the heart of Ryde. Fireworks, rides, food and carols.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="15th Dec 2019 - Join us for an authentic carols experience at Sydney's second largest Christmas carols in the heart of Ryde. Including fireworks, rides, food and carols."/>
    <title>Carols on the Common | Christmas Carols in North Ryde | 15th Dec 2019</title>
    <?php include_once("inc/head.php"); ?>
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.css" type="text/css" rel="stylesheet" />
    </head><!--/head-->

<body class="home">
	<?php include_once("inc/header.php"); ?>
	<!--/#home-->
	<section id="explore">
		<div class="container">
			<div class="row">			
				<div class="col-md-4 col-md-offset-1 col-sm-5">
					<h2>The carols begin in</h2>
				</div>				
				<div class="col-sm-7 col-md-6">					
					<ul id="countdown">
						<li>					
							<span class="days time-font">00</span>
							<p>days </p>
						</li>
						<li>
							<span class="hours time-font">00</span>
							<p class="">hours </p>
						</li>
						<li>
							<span class="minutes time-font">00</span>
							<p class="">minutes</p>
						</li>
						<li>
							<span class="seconds time-font">00</span>
							<p class="">seconds</p>
						</li>				
					</ul>
				</div>
			</div>
			<?php
			$date = time();
			$eventStart = strtotime('2019-12-12 00:00:00');
			if ($date > $eventStart) { ?>
				<div class="row">			
					<div class="col-sm-4  col-sm-offset-2">
						<a href="/program" class=" btn btn-primary btn-full-width blue">Program & Times</a>
					</div>
					<div class="col-sm-4 ">
						<a href="/lyrics"  class=" btn btn-primary  btn-full-width green">Sing Along to Carols</a>
					</div>
					
				</div>
				<div class="row">			
					<div class="col-sm-4  col-sm-offset-2">
						<a href="/sponsors"  class=" btn btn-primary btn-full-width yellow">Sponsors</a>
					</div>
					<div class="col-sm-4 ">
						<a href="/donate"   class=" btn btn-primary  btn-full-width orange">Donate</a>
					</div>
					
				</div>
			<?php } ?>
		</div>
	</section>

	<section class="container" id="about">
		<div class="row">
			<!-- <div class="col-sm-12">
				<center>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/QJYqAQB7JbE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>				
				<p><strong>The highlight reel from the Rotary Carols on the Common.</strong><BR> Thank you to everyone who supported us in making the night such a success! A special thank you to sponsor <a href="www.sydneyclassic.com.au">Sydney Classic Productions</a> for their video expertise on the night and producing this fantastic video for us all</p>
				</center>
				<HR>
			</div> -->

			<div class="images col-sm-6 clearfix">
				<img class="img-responsive" src="images/candid/1.jpg" alt="Girls playing">
				<img class="img-responsive" src="images/candid/2.jpg" alt="Fireworks">
				<!--img class="img-responsive" src="images/candid/3.jpg" alt="Rides"-->
				<!--img class="img-responsive" src="images/candid/6.jpg" alt="Star People"-->
				<img class="img-responsive" src="images/candid/5.jpg" alt="Girls at Carols">
				<img class="img-responsive" src="images/candid/4.jpg" alt="Girls in Santa costume">
				<img class="img-responsive" src="images/candid/collection.jpg" alt="Rotary Carols Bucket Collection">
				<img class="img-responsive" src="images/candid/crowd.jpg" alt="Crowd at the Rotary Carols">
				<img class="img-responsive" src="images/candid/crowdatnight.jpg" alt="Crowd at Rotary Carols on the Common at Twilight">
				<img class="img-responsive" src="images/candid/dancers.jpg" alt="Dancers at Rotary Carol on the Common">
				<img class="img-responsive" src="images/candid/rides.jpg" alt="Rotary Carols on the Common rides">
				<img class="img-responsive" src="images/candid/santa.jpg" alt="Santa visiting at the Rotary Carols on the Common">
				<img class="img-responsive" src="images/candid/singers.jpg" alt="Carols singers at the Rotary Carols on the Common">
				<img class="img-responsive" src="images/candid/stage.jpg" alt="The Rotary Carols stage">
				<img class="img-responsive" src="images/candid/stars.jpg" alt="Star People">

			</div>
		
			<div class="about-content col-sm-6">					
				<h2>Carols on the Common 2019</h2>
				<h3>15th December 2019</h3>	
				<h3>Carols by Candlelight - Fireworks - Rides - Food Trucks</h3>	
				<p>Join us for an authentic carols experience at Sydney's second largest Christmas carols in the heart of Ryde.</p>
				<p>Kicking off at 5:30pm and running through the night is the <em>Carols on the Common</em> performance, featuring all your favourite classic carols with the best performers from all over Sydney. To close out the night, the whole family will be treated to an awe-inspiring firework display.</p>
				<p>Come in early to experience our amusement activities and keep the kids entertained with the giant slide, or dodgem cars and dozens of other rides. Food vendors and a sausage sizzle will be serving throughout the night. </p>
				<P>Limited parking is available on-site with a gold coin donation.</P>
				<P>Money raised on the night goes to worth causes such as ROMAC - an organisation bringing sick and ill children from nations all over the world for life changing surgery in Australia. With your help we can raise over $10 000 this year for ROMAC alone.</P>
				
				<h3>Carols by Candlelight - Fireworks - Rides - Food trucks</h3>
				
				
				<h4>15th December 2019</h4>
				<h4>from 5:30 pm - 9:30 pm</h4>
				<h4>North Ryde Commons</h4>
				<h4>Cnr Wicks & Twin Road, North Ryde</h4>
	
				<section class="container-fluid">
					<div class="row">
						<div class="col-sm-4 col-xs-4">
							<a href="#" data-featherlight="images/poster/2019/poster1.jpg">
								<img src="images/poster/2019/poster1tn.jpg" alt="Carols on the Common Main poster" width="100%">View Larger</a>
		
						</div>
						<!--div class="col-sm-4 col-xs-4">
							<a href="#" data-featherlight="images/poster/2018/poster2.jpg">
								<img src="images/poster/2018/poster2tn.jpg" alt="Carols on the Common Full poster"  width="100%">View Larger</a>
						</div-->
						<!--div class="col-sm-4 col-xs-4"><a href="#" data-featherlight="images/poster/poster3.jpg">
							<img src="images/poster/poster3th.jpg" alt="Carols on the Common sponsors poster"  width="100%">View Larger</a>
						</div-->
					</div>
				</section>

				<section class="container-fluid">
					<div class="row">
						<div class="col-sm-12">
						
							<a href="/getting-there" class="btn btn-primary btn-full-width">View Location<i class="fa fa-angle-right"></i></a>

						</div>
					</div>
					<!-- <a href="#" data-featherlight="images/poster/2016/weather.png"><BR>
					<img src="images/poster/2016/weather.png" alt="Carols on the Common Weather relocation plan"  width="100%">View Larger</a>
					<P>WET WEATHER UPDATE: The forecast is looking great for Sunday and the carols will go ahead as planned. The precise location of the carols will be slightly shifted away from Wicks Rd and into the Macquarie Hospital carpark opposite Nerang St.</P>

					<P>All your favourite rides, games, food stalls, activities and entertainment will proceed as usual. The preshow begins at 5:30pm and the carols commence at 8pm followed by an awesome fireworks display at 9:15pm. We can’t wait to see you there. 
					</P> -->
					<div class="row">
						<div class="col-sm-6 col-xs-6">
							<a href="https://www.facebook.com/events/194275988193771" class=" btn btn-primary btn-full-width" target="_blank" style="background-color:#3B5998;">RSVP to Facebook Event</a>
						</div>
						<div class="col-sm-6 col-xs-6">
							<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//www.facebook.com/events/194275988193771/" class=" btn btn-primary btn-full-width" target="_blank" style="background-color:#3B5998;">Share on Facebook</a>
						</div>
					</div>
				</section>

			</div>
		</div>	
	</section>
	
	<section id="map">
		<div id="map">
			<a href="https://www.google.com.au/maps/dir//North+Ryde+Common,+North+Ryde+NSW+2113/@-33.7995619,151.1251479,17z/data=!4m13!1m4!3m3!1s0x6b12a5ebf45079e5:0xdc691e317e2eddc0!2sNorth+Ryde+Common,+North+Ryde+NSW+2113!3b1!4m7!1m0!1m5!1m1!1s0x6b12a5ebf45079e5:0xdc691e317e2eddc0!2m2!1d151.1224793!2d-33.8015959" target="_blank">
				<div id="gmap-wrap">
					<div id="gmap"> 				
					</div>	 			
			    </div>
			</a>
		</div>
	</section>
	
	<section class="email" id="email">

			<!-- Begin MailChimp Signup Form -->
		<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
			/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
			   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
		</style>
		<div id="mc_embed_signup">
		<form action="//carolsonthecommon.us14.list-manage.com/subscribe/post?u=c8500017cdb4332fd1b4034c1&amp;id=a018f887df" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		    <div id="mc_embed_signup_scroll">
			<label for="mce-EMAIL">Be the first to know about next year's carols... </label>
			<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
		    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c8500017cdb4332fd1b4034c1_a018f887df" tabindex="-1" value=""></div>
		    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
		    </div>
		</form>
		</div>

	<!--End mc_embed_signup-->
	</section>

	<section id="event">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div id="event-carousel" class="carousel slide" data-interval="2000">
						<h2 class="heading">Highlights of the Carols</h2>
						<a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<div class="col-xs-6 col-md-4">
										<div class="single-event">
											<img class="img-responsive" src="images/candid/7.JPG" alt="event-image">
											<h4>Live Performaces</h4>
											<p>Put out a picnic rug and join us for the peaceful sounds of Christmas carols and live entertainment, starting from 5:30pm. </p>
										</div>
									</div>
									<div class="col-xs-6 col-md-4">
										<div class="single-event">
											<img class="img-responsive" src="images/candid/3.jpg" alt="event-image">
											<h4>Amusements & rides</h4>
											<p>Spend the afternoon perusing the various stalls and amusements or let your hair down with our rides and giant slide.</p>
										</div>
									</div>
									<div class="col-xs-6 col-md-4">
										<div class="single-event">
											<img class="img-responsive" src="images/candid/2.jpg" alt="event-image">
											<h4>Fireworks</h4>
											<p>See the fireworks display at the end of the night's carol performances.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>			
		</div>
	</section><!--/#event-->

	    
    <!--section id="sponsor">
		<div id="sponsor-carousel" class="carousel slide" data-interval="false">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2>Sponsors</h2>			
						<a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item">
								<ul>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>
								</ul>
							</div>
							<div class="item active">
								<ul>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png" alt=""></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>				
			</div>

		</div>
	</section>-->
	<!--/#sponsor-->

	<div class="container" style="padding-top:30px;">
	    <div id="facebook-box">
		    <div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-page" data-href="https://www.facebook.com/RotaryCarolsOnTheCommon" data-width="500" data-height="300" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/RotaryCarolsOnTheCommon"><a href="https://www.facebook.com/RotaryCarolsOnTheCommon">Rotary Carols on the Common</a></blockquote></div></div>
	    </div>
	</div>

	<?php include_once("inc/footer.php"); ?>
    <script src="//code.jquery.com/jquery-latest.js"></script>
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.3.5/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>
