<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Voluteer to help at this year's Carols on the Common event in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Voluteer to help at this year's Carols on the Common event in North Ryde, 15th Dec 2019."/>
    
    <title>Sponsorship Invoice | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once("inc/head.php"); ?>
    </head><!--/head-->

<body class="volunteers page">
	<?php include_once("inc/header.php"); ?>

	<div class="main-container xmas-lights" role="main">
	
		<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					<h1 class="large mb16 mb-xs-24">Sponsorship Request</h1>
						<p class="heading-line heading-line-lime super-lead">Thanks for sponsoring this year's carols.</p>
						<!-- <p class="lead"></p> -->
				</div>
			</div>
		</div>
	</section>
		
		<section class="promo-block promo-block-red">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-md-push-7">
						<h2>Instructions</h2>
						<p>Thank you for sponsoring this year's Carols on the Common.  Please assist by filling in the form and we will email you an invoice.</p>
						<p>If you wish to change your sponsorship level please complete the form again.</p>
						<p><script type="text/javascript">document.write('Please contact TBD');</script> if you have any questions.</p>
					</div>
					<div class="col-md-7 col-md-pull-5">
						<? if (file_exists('../lib')): ?>
								<? if (isset($_GET['thanks'])): ?>
									<? include 'inc/invoice_thanks.php' ?>
								<? else: ?>
									<? include 'inc/invoice_form.php' ?>
								<? endif ?>
						<? else: ?>
								<p>(Sponsorship Invoice form goes here)</p>
						<? endif ?>
					</div>
				</div>
			</div>
		</section>
		<hr> 
	</div>
	<?php include_once("inc/footer.php"); ?>
</body>
</html>
