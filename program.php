<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Check out our program so you don't miss a moment of this year's Carols on the Common.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Check out our program so you don't miss a moment of this year's Carols on the Common."/>
    
    <title>Program for the Night | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/head.php");
		 ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/header-no-slider.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3 mobile-sidebar">
						<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/mobile-sidebar.php"); ?>						
					</div>	
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h1 class="large mb16 mb-xs-24">Program</h1>
								<!-- <p class="heading-line heading-line-lime super-lead">We're busily preparing the program for this year's Carols on the Common. In the meantime, take a look over last year's program below.</p> -->
								<p class="heading-line heading-line-lime super-lead">Don't miss a moment of this year's carols - Check out our program for the evening.</p>
								<table class="table program table-striped">
									<tbody>
										<tr>
							                <td class="time">5:30 pm</td>
							                <td class="description"><strong>Pre-show Entertainment Begins</strong>: Master of Ceremonies Stephen Sim will introduce the pre-show entertainment and other housekeeping information.</td>
							            </tr>
							            <tr>
							                <td class="time">5:31 pm</td>
							                <td class="description"><strong>Royal Rehab Lifestyle Choir</strong>: Choir performance from the Royal Rehab Lifestyle Choir - A choir comprised of patients from the local Royal Rehab Clinic, with their families and friends who sing as a form of recreational therapy.</td>
							            </tr>
							            <tr>
							                <td class="time">5:55 pm</td>
							                <td class="description"><strong>Santa & Guests</strong>: Santa Claus makes an early appearance! He is accompanied by Sing Stars, and the TWT Granny Smith Festival Queen. </td>
							            </tr>
							            <tr>
							                <td class="time">6:05 pm</td>
							                <td class="description"><strong>Youth Choir and orchestra Sydney</strong>: Music from the Bennelong Music group.</td>
							            </tr>
							            <!-- <tr>
							                <td class="time">6:10 pm</td>
							                <td class="description"><strong>Sing Star Winners</strong>: Winners of the local Sing Star competition, will perform their winning songs to the audience.</td>
							            </tr> -->
							            
							            <tr>
							                <td class="time">6:35 pm</td>
							                <td class="description"><strong>Davin Griffiths-Jones School of Performing Arts</strong>: Choreographed performances from the Davin Griffiths-Jones School.</td>
							            </tr>
							            <tr>
							                <td class="time">7:00 pm</td>
							                <td class="description"><strong>MC Steve Sim Annoucements</strong>:</td>
							            </tr>
							            <tr>
							                <td class="time">7:05 pm</td>
							                <td class="description"><strong>Rock & Soul Choir</strong></td>
							            </tr>
							            <tr>
							                <td class="time">7:30 pm</td>
							                <td class="description"><strong>Bucket collection for NSW Fires</strong>: A moment silence will be observed for the victims of NSW fires. We will be supported by volunteers from the Terrey Hills Rural Fire Service, who will bring their firefighting vehicle and participate in the bucket collection to raise awareness and funds.</td>  
							            </tr>
							            <tr>
							                <td class="time">7:40 pm</td>
							                <td class="description"><strong>"After 5" Performance</strong>:</td>  
							            </tr>
							            <tr>
							                <td class="time">7:50 pm</td>
							                <td class="description"><strong>Video</strong>:</td>  
							            </tr>
							            <tr>
							                <td class="time">8:00 pm</td>
							                <td class="description"><strong>Official Carols Begin</strong>: Stephen Sim, the master of ceremonies, officially opens the 26th Annual Carols on the Common.<BR>
								                 Carols lead by : Roseanna Gallo, Cheryn Vlazny, Eleanor Taig, John Smolders, Jane Meney & Jordyn Richards and supported by the Leichhardt Celebrity Brass Band and Macquarie Performing Arts Dancers.<BR>

								                 <P>Follow along with the carols with the <a href="/lyrics">lyrics for each song.</a></P>
		</td> 
							            </tr>
							            <tr>
							                <td class="time">9:00 pm</td>
							                <td class="description"><strong>Santa Claus and other guests arrive on stage!</strong>: A special Christmas blessing is provided to round out the night from Pastor John Chappell of St John's Anglican Church North Ryde.
		</td>
							            </tr>
							            <tr>
							                <td class="time">9:15 pm</td>
							                <td class="description"><strong>Fireworks commence.</strong>
		</td>
							            </tr>
							            <tr>
							                <td class="time">9:30 pm</td>
							                <td class="description"><strong>The end of Carols on the Common for 2019. See you next year!</strong>
		</td>
					            		</tr>
									</tbody>
					    		</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<hr>
	
		
	</div>

		<?php include_once( $_SERVER['DOCUMENT_ROOT'] . "/inc/footer.php"); ?>
</body>
</html>