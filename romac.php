<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Learn about ROMAC - The charity who receives all money raised at the Carols on the Common night.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Learn about ROMAC - The charity who receives all money raised at the Carols on the Common night."/>
    
    <title>ROMAC the Charity | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once("inc/head.php"); ?>
    </head><!--/head-->

<body class="romac page">
	<?php include_once("inc/header.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
    	<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center">
						<h1 class="large mb16 mb-xs-24">Charity</h1>
						<p class="heading-line heading-line-lime super-lead">Money raised at the Carols on the Common goes to worthy causes like ROMAC.</p>
						<p class="lead">
							ROMAC is the Rotary Oceania Medical Aid for Children. ROMAC, with the generous assistance of many eminent Australian and New Zealand surgeons, provide life-changing surgery to more than 400 children across 20 developing countries. Without generous donations like yours, ROMAC would not be able to carry on this vital work. Thank you. Read some of their stories below.
						</p>
						<hr>
						<a href="/donate"  class=" btn btn-primary">Donate Now</a>
						<hr>
					</div>
				</div>
			</div>
		</section>
	
		<section class="promo-block promo-block-green">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-6 col-sm-6 col-md-push-6 col-sm-push-6 col-xs-12">
						<img src="http://carolsonthecommon.org.au/images/romac/REX_KUMO.JPG" width="653">
					</div>
					<div class="col-md-6 col-sm-6 col-md-pull-6 col-sm-pull-6 col-xs-12">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Rex's story</h2>
							<p>Five year old Rex and his mother Karen live in the Papua New Guinea Highlands, a 10 hour bus ride from the nearest town. Rex was also born with a malformation known as an encephalocele (a herniated brain). Because of this deformity, Rex was ostracized and unable to attend school.</p>
							<P>When ROMAC learnt of his case, they stepped in. Rex was flown to Adelaide and was operated on by Dr. Mark Moore at the Women's and Children's Hospital. Rex made a speedy recovery and within two weeks he was organising miniature games of Rugby Union in the corridors of the hospital! After the surgery, Central Region’s Oleh Bilyk said “Rex will now be able to live a normal life and even fulfil his dream of going to school like the other children."</P>
						</div>
					</div>
				</div>
			</div>
			<!--end of container-->
		</section>


		<section class="promo-block promo-block-red">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<img src="http://carolsonthecommon.org.au/images/romac/gregory-2.jpg" alt="" width="653">
						<img src="http://carolsonthecommon.org.au/images/romac/gregory-3.jpg" alt="" width="653">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Greg's story</h2>
							<p>Six-year-old Gregory Jack had not closed his eyes for two years. Even when sleeping, his eyes only half close, meaning he wakes up with sore, red eyes.</p>
									
							<P>Gregory has a genetic disorder called Crouzon's syndrome; his skull and facial bones have been unable to expand as he has grown. He needed surgery to cut and release his facial bones to bring his face forward. Two years later, ROMAC raised the $40,000 required for Gregory's treatment.</P>
							<P>Greg's uncle, Mr Vilen, accompanied him to Melbourne. 'If he stayed there he would stay how he is. It would only be through God's grace that he would live.". Gregory has progressed well since his initial operation and is looking forward to going home to see his friends again. </p> 
									
						</div>
					</div>
				</div>
			</div>
		</section>


		<section class="promo-block promo-block-teal">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-6 col-sm-6 col-md-push-6 col-sm-push-6 col-xs-12">
						<img src="http://carolsonthecommon.org.au/images/romac/baby-nelia.jpg" alt="" width="653">
					</div>
					<div class="col-md-6 col-sm-6 col-md-pull-6 col-sm-pull-6 col-xs-12">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Baby Nelia</h2>
							<p>Baby Nelia had been unable to feed or digest her food due to a rare birth defect that left her with a malformed oesophagus. Without surgery to correct the condition - which doctors in East Timor lack the specialist skills to perform - Nelia had little more than a week to live.</P>
							<P>But a mercy call to ROMAC has saved Nelia's life. She had surgery at the Monash Children's Hospital in Melbourne and within five days of surgery, doctors were confident the two-week-old would make a full recovery.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<hr>
						<a href="/donate" class=" btn btn-primary">Donate Now</a>
						<hr>
					</div>
				</div>
			</div>
		</section>
	</div>

		<?php include_once("inc/footer.php"); ?>
</body>
</html>