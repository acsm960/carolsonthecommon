<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Schedule for Carols on the Common, in Ryde. Fireworks, rides, food trucks and carols.">
  
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="15th Dec 2019 - Schedule for Carols on the Common, in Ryde. Including fireworks, rides, twilight markets, and carols."/>
    <title>Schedule of Events | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once("inc/head.php"); ?>
    </head><!--/head-->

<body class="schedule page">
	<?php include_once("inc/header.php"); ?>
	<!--/#home-->

	<div class="main-container xmas-lights" role="main">
		
		<section>
			<div class="container pt56 pt-sm-40">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center">
						<h1 class="large mb16 mb-xs-24">Schedule</h1>
						<p class="heading-line heading-line-lime super-lead">
							What you need to know</p>
						<p class="lead">
							Join us for an authentic carols experience at Sydney's second largest Christmas carols in the heart of Ryde. The festivities kick off at 5:30 pm and finish with the spectacular firework show at 9:15 pm.</p> 
							<P><a href="/program">See a detailed copy of the 2018 program.</a></p>
					</div>
				</div>
			</div>
		</section>
		
		
		<section class="promo-block promo-block-green">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-7 col-sm-6 col-md-push-5 col-sm-push-6 col-xs-12">
						<img src="http://carolsonthecommon.org.au/images/candid/live-entertainment.JPG" alt="" width="653">
					</div>
					<div class="col-md-5 col-sm-6 col-md-pull-7 col-sm-pull-6 col-xs-12">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Live Entertainment</h2>
							<p>Start the evening early by grabbing the best spot on the Commons and enjoying the live entertainment. Santa has been known to make an early appearance to children who arrive early! </p>
							<p class="schedule-time" href="/vantage-points/">from 5:30 pm</p>
							<a href="/program" class=" btn btn-primary btn-full-width">View Full Schedule</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!--section class="promo-block promo-block-green">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-7 col-sm-6 col-md-push-5 col-sm-push-6 col-xs-12">
						<img src="http://carolsonthecommon.org.au/images/candid/markets.jpg" alt="" width="653">
					</div>
					<div class="col-md-5 col-sm-6 col-md-pull-7 col-sm-pull-6 col-xs-12">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Twilight Markets</h2>
							<p>The carols night kicks off with twilight Christmas markets from the early afternoon. Browse the boutique stalls for last minute Christmas shopping.</p>
							<p class="schedule-time" href="/vantage-points/">4:30 pm - 6:30 pm</p>
						</div>
					</div>
				</div>
			</div>
		</section-->
		
		<section class="promo-block promo-block-red">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-7 col-sm-6 ">
						<img src="http://carolsonthecommon.org.au/images/candid/7.JPG" alt="" width="653">
					</div>
					<div class="col-md-5 col-sm-6 ">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Christmas Carols</h2>
							<p>Join us for a night of fun filled carols, starting at on the North Ryde Common just after 8:00 pm.  Don't forget to pack a picnic rug and a few snacks.</p> 
							<P> There might even be a special guest visiting from the north pole!</p>
							<p class="schedule-time">8:00 pm - 9:00 pm</p>
							<a href="/program" class=" btn btn-primary btn-full-width">View Full Schedule</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="promo-block promo-block-teal">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-7 col-sm-6 col-md-push-5 col-sm-push-6">
						<img src="http://carolsonthecommon.org.au/images/candid/2.jpg" alt="" width="653">
					</div>
					<div class="col-md-5 col-sm-6 col-md-pull-7 col-sm-pull-6">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Fireworks</h2>
							<p>The carols come to a spectacular finish with a fireworks display at 9:15 pm</p>
							<p class="schedule-time">9:15 pm</p>
							<a href="/program" class=" btn btn-primary btn-full-width">View Full Schedule</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<hr> 
		
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center">
						<h1 class="large mb16 mb-xs-24">All Day</h1>
						<p class="heading-line heading-line-lime super-lead">Several rides and food vendors will be present at the carols for the entire event.</p>
					</div>
				</div>
			</div>
		</section>
		
		<section class="promo-block promo-block-red">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-7 col-sm-6 ">
						<img src="http://carolsonthecommon.org.au/images/candid/dogem-cars.JPG" alt="" width="653">
					</div>
					<div class="col-md-5 col-sm-6 ">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Rides & Games</h2>
							<p> Rides will be open at the carols from early in the afternoon till late at night. Giant slide, dodgem-cars - all your favourites will keep the kids entertained for hours. </p>
							<P>This year will include Swinging chairs (large & small), Simulation Rocket, Cups & Saucers, Dodgem cars, Jumping Castle and Giant slide.</P>
							<P>Don't forget the Laughing Clowns, Lucky Numbers and the Strongman Strength Tester as well!</P>
							<p class="schedule-time">From 5:30 pm - 9 pm </p>
						</div>
					</div>
				</div>
			</div>
		</section>
			
		<section class="promo-block promo-block-orange">
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-7 col-sm-6 col-md-push-5 col-sm-push-6">
						<img src="http://carolsonthecommon.org.au/images/candid/8.jpg" alt="" width="653">
					</div>
					<div class="col-md-5 col-sm-6 col-md-pull-7 col-sm-pull-6">
						<div class="promo-block-inner text-center">
							<h2 class="heading-line">Food Vendors</h2>
							<p>Feeling peckish? We have dozens of different food vendors lined up so there are plenty of great food and drink options available.</p>
							<P>Sausage sizzle, steak sandwich, delicious German Kranskys with Sauerkraut, Gözleme, Kebabs, Ice Cream, Coffee Fairfloss, Dutch Pancakes, Potatoes on a stick and much more. </P>
							<P>Rotary will also be running a sausage sizzle with all money going to support sick children through ROMAC.</P>
							<p class="schedule-time">From 5:30 pm</p>
						</div>
					</div>
				</div>
			</div>
		</section>
    </div>
    

	<?php include_once("inc/footer.php"); ?>
</body>
</html>