<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Rotary Carols on the Common in Ryde is not possible without the help of many generous and dedicated sponsors. ">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Rotary Carols on the Common in Ryde is not possible without the help of many generous and dedicated sponsors.."/>
    
    <title>Sponsors | Christmas Carols in North Ryde | 15th Dec 2019</title>
    <?php include_once("inc/head.php"); ?>
    </head><!--/head-->

<body class="sponsors page">
	<?php include_once("inc/header.php"); ?>
	<div class="main-container xmas-lights sponsors" role="main">
    	<section class="container">
	    	<div class="row">
		    	<div class="col-sm-10 text-center">
					<h1 class="large heading-line">Sponsors</h1>
		      		<p class="lead">Rotary Carols on the Common in Ryde is not possible without the help of many generous and dedicated sponsors. If you are interested in becoming a sponsor please <a href="mailto:sponsorship@carolsonthecommon.org.au">contact us</a> or download our <a href="CarolsSponsorPack2019.pdf">sponsorship package</a>.</p>
		      	</div>
	      	</div>
	      	<div class="row">			
					<div class="col-sm-4 ">
						<a href="/CarolsSponsorPack2019.pdf"  class=" btn btn-primary btn-full-width">Download Sponsorship Pack</a>
					</div>
				</div>
	      	<hr>
	    </section ><!-- END HEADER -->
	    
<!--
	    <section id="sponsors-platinum" class="container sponsors">
	    	<div class="row">
		    	<div class="col-sm-10 text-center">
					<h2 class="large color-green">Platinum Sponsor</h2>
		    	</div>
	      	</div>
	      	<div class="row row-eq-height text-center"> 
	      		<a href="https://www.jnj.com.au/" target="_blank" class="col-sm-8 sponsor-block">
					<img src="images/sponsors/Johnson-Johnson.jpg" alt="Johnson & Johnson" class="sponsor-logo">
			      	<p>Johnson & Johnson</p>
			    </a>  	
	      	</div>
	      	<hr>
	    </section>
-->
	
	    <section id="sponsors-gold" class="container sponsors">
	    	<div class="row">
		    	<div class="col-sm-10 text-center">
					<h2 class="large color-yellow">Gold Sponsors</h2>
		    	</div>
	    	</div>
	    			       	
		  	<div class="row row-eq-height text-center" style="">
		    	<a href="http://coxsroadmall.com.au/" target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
					<img src="images/sponsors/CoxsRdMall.jpg" alt="Coxs Road Mall Logo" class="sponsor-logo">
					<p>Coxs Road Mall</p>
		      	</a>
		      	<a href="http://www.northrydersl.com.au/" target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
				    <img src="images/sponsors/northryderslLogo.jpg" alt="North Ryde RSL" class="sponsor-logo">
			      	<p>North Ryde RSL</p>	
		      	</a>
		      	<a href="http://www.ryde.nsw.gov.au/"  target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
			    	<img src="images/sponsors/Ryde_City_Council.png"  alt="Ryde City Council"  class="sponsor-logo">
					<p>City of Ryde</p>
	      		</a>
	      		<a href="http://www.sydneyclassic.com.au/" target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
				    <img src="images/sponsors/Sydney_Classic.png" alt="Sydney classic Productions logo" class="sponsor-logo" style="padding: 60px 0px;">
			      	<p>Sydney Classic Productions</p>
			    </a>
			    <a href="http://raywhitenorthryde.com.au/" target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
				    <img src="images/sponsors/raywhitelogo.png"  alt="Ray White North Ryde | Macquarie Park"  class="sponsor-logo">
			    	<p class="sponsor-title">Ray White North Ryde | Macquarie Park</p>	
			    </a>

	    	</div>	      	
	      	<hr>
	    </section>
		      	
		<section id="sponsors-silver" class="container sponsors">
	    	<div class="row">
		    	<div class="col-sm-10 text-center">
					<h2 class="large color-orange">Silver Sponsors</h2>
		      	</div>
	      	</div>
	      	<div class="row row-eq-height text-center">
	 			<a href="https://www.eventcinemas.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/eventcinema.png"  alt="Event Cinema"  class="sponsor-logo">
			    	<p class="sponsor-title">Event Cinema Macquarie Centre</p>	
			    </a>
			    <a href="https://www.novartis.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
					<img src="images/sponsors/novartis.jpg" alt="Novartis Australia"  class="sponsor-logo">
					<p>Novartis Australia</p>
		      	</a>
				<a href="https://www.bendigobank.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/bendigobank.jpg"  alt="Bendigo Bank"  class="sponsor-logo">
			    	<p class="sponsor-title">North Ryde Community Bank Branch®</p>
		      	</a>
		      	<a href="http://www.sascodevelopments.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/SascoLogo.png" alt="Sasco Developments"  class="sponsor-logo">
			    	<p class="sponsor-title">Sasco Developments</p>
		      	</a>	
		      	<a href="http://freshmark.com.au/service/sydney-market-foundation/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
					<img src="images/sponsors/Sydney-markets.jpg" alt="Sydney Markets Foundation"  class="sponsor-logo">
					<p>Sydney Markets Foundation</p>
		      	</a> 
<!--
		      	<a href="http://www.epochtimes.com.au" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/The-Epoch-Times.jpeg" alt="The Epoch Times" class="sponsor-logo">
			      	<p>Epoch Times</p>
		      	</a>
-->
		    	<!--<a href="http://www.urbangrowth.nsw.gov.au/work/projects/lachlans-line-macquarie-park.aspx" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/lachlans-lien.jpg" alt="Lachlan's Line" class="sponsor-logo">
			    	<p class="sponsor-title">Lachlan's Line</p>	
			    </a>
<!--
			    <a href="https://www.bresicwhitney.com.au" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/bresicwhitney.gif"  alt="Bresic Whitney"  class="sponsor-logo">
			    	<p class="sponsor-title">Bresic Whitney</p>	
			    </a>
-->
			    </a>
		      	
<!--
		      	<a href="http://paragonrisk.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/Paragon.jpg" alt="Paragon Risk Management" class="sponsor-logo">
			      	<p>Epoch Times</p>
		      	</a>
-->
		      	
	      	</div>	      		      	
	      	<hr>
	    </section>
	    
	    <section id="sponsors-bronze-plus" class="container sponsors">
	    	<div class="row">
		    	<div class="col-sm-10  text-center">
		      		<h2 class="large color-red">Bronze + Sponsors</h2>
		      	</div>
	      	</div>
	      	<div class="row row-eq-height text-center">
		    	<a href="http://www.driessen.com.au/" target="_blank" class="col-sm-4 col-xs-6  sponsor-block">
					<img src="images/sponsors/driesseninsurance.png"  alt="Driessen Insurance" class="sponsor-logo">
			    	<p class="sponsor-title">Driessen Insurance</p>
		    	</a>
		    	<a href="http://www.actionsound.com.au/" target="_blank" target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
				    <img src="images/sponsors/Action-Sound.jpg" alt="Action Sound" class="sponsor-logo">
			      	<p>Action Sound</p>
		      	</a>
<!--
		      	<a href="http://www.barcoco.com.au/" target="_blank" class="col-sm-4 col-xs-6 sponsor-block">
				    <img src="images/sponsors/barcoco.jpg" alt="Bar Coco"  class="sponsor-logo">
			    	<p class="sponsor-title">Bar Coco</p>
		      	</a>	
-->     

		    		
	      	</div>
	      	<hr>
	    </section>
	     
	    <section id="sponsors-bronze" class="container sponsors">
	    	<div class="row">
		    	<div class="col-sm-10 text-center">
					<h2 class="large color-teal">Bronze Sponsors</h2>
		    	</div>
	      	</div>
	      	
	      	<div class="row row-eq-height text-center" style="">
	      		<a href="http://www.mitsubishielectric.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/me.png" alt="Mitsubishi Electric"  class="sponsor-logo">
			    	<p class="sponsor-title">Mitsubishi Electric</p>
		      	</a>
		      	

		      	
		    	 <a href="" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/lush.jpg"  alt="The lush Collective hair + skin (North Ryde)"  class="sponsor-logo">
			    	<p class="sponsor-title">The lush Collective hair + skin (North Ryde)</p>	
			    </a><!--
			    <a href="http://www.barcoco.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/barcoco.jpg" alt="Bar Coco"  class="sponsor-logo">
			    	<p class="sponsor-title">Bar Coco</p>
		      	</a>-->
		      	
		      	<!-- <a href="http://www.eastwoodshoppingcentre.com.au/index.php" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
					<img src="images/sponsors/eastwoodshoppingcentre.png" alt="Eastwood Shopping Centre"  class="sponsor-logo">
					<p>Eastwood Shopping Centre</p>
		      	</a> -->
		      	

		      	<a href="https://www.konicaminolta.com/Australia" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				     <img src="images/sponsors/KM.jpg"  alt="Konica Minolta"  class="sponsor-logo">
			    	<p class="sponsor-title">Konica Minolta</p>
		      	</a>

		      	<a href="http://www.elytrart.com/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				     <img src="images/sponsors/elytrart.png"  alt="Elytrart Logo"  class="sponsor-logo">
			    	<p class="sponsor-title">Elytrart / Sign Your Wall</p>
		      	</a>

		      	
<!--
		      	<a href="" target="_blank" class="col-sm-3  col-xs-6 sponsor-block">
					<p class="sponsor-logo" >Combined Chambers of Commerce</p>
			    	<p class="sponsor-title">Eastwood Chamber of Commerce, Riverside Business Chamber</p>	
			    </a>
-->
		      	<!--<a href="" target="_blank" class="col-sm-3  col-xs-6 sponsor-block">
					<p class="sponsor-logo" >Combined Chambers of Commerce</p>
			    	<p class="sponsor-title">NSW Business Chamber, Ryde Business Forum, Macquarie Park Chamber of Commerce, Eastwood Chamber of Commerce, Gladesville Chamber of Commerce.</p>	
			    </a>
			    <a href="http://www.landmarkhotel.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
					<img src="images/sponsors/landmark.png" alt="Landmark Hotel Eastwood"  class="sponsor-logo">
					<p>Landmark Hotel - Eastwood</p>
		      	</a> -->
		      	
		      	
		      	
	      	</div>     	
	      	<hr>
	    </section>
	    
	    <section id="sponsors-elves" class="container sponsors">
	      	<div class="row">
		      	<div class="col-sm-10 text-center">
		      		<h2 class="large color-orange">Lost Elves Tent</h2>
		      	</div>
	      	</div>
	      	
			<div class="row row-eq-height text-center" style="">
		      	
				<a href="https://www.bendigobank.com.au/" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/bendigobank.jpg"  alt="Bendigo Bank"  class="sponsor-logo">
			    	<p class="sponsor-title">North Ryde Community Bank Branch®</p>
		      	</a>
	      	</div> 	
	      	<hr>
	    </section>
	    
	    

		<section id="sponsors-royalrehab" class="container sponsors">
	      	<div class="row">
		      	<div class="col-sm-10 text-center">
		      		<h2 class="large color-teal">Stroke NSW & Royal Rehab Community Tent</h2>
		      	</div>
	      	</div>
	      	
			<div class="row row-eq-height text-center" style="">
		      	
				<a href="http://www.releagues.com.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/releagues.png"  alt="Ryde Eastwood Leagues Club"  class="sponsor-logo">
			    	<p class="sponsor-title">Ryde Eastwood Leagues Club</p>	
			    </a>
		      	
 				
			    <a href="http://www.sascodevelopments.com.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/SascoLogo.png" alt="Sasco Developments"  class="sponsor-logo">
			    	<p class="sponsor-title">Sasco Developments</p>
		      	</a>
			    
	      	</div> 	
	      	<hr>
	    </section>


	    <section id="sponsors-rasaid" class="container sponsors">
	      	<div class="row">
		      	<div class="col-sm-10 text-center">
		      		<h2 class="large color-green">RASAID Community Tent</h2>
		      	</div>
	      	</div>
	      	
			<div class="row row-eq-height text-center" style="">
		      	
				<!-- <a href="http://www.releagues.com.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/releagues.png"  alt="Ryde Eastwood Leagues Club"  class="sponsor-logo">
			    	<p class="sponsor-title">Ryde Eastwood Leagues Club</p>	
			    </a> -->
			    
		      	<a href="http://www.victordominello.com.au" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/victordominello.png" alt="Victor Dominello"  class="sponsor-logo">
				    <p class="sponsor-title">Victor Dominello</p>	
			    </a>
			    <a href="http://johnalexander.net.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/johnalexander.png" alt="John Alexander" class="sponsor-logo">
			    	<p class="sponsor-title">John Alexander</p>	
			    </a>
			    <a href="http://ladyannefunerals.com.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/Lady_Anne_Funerals.jpg" alt="Lady Anne Funerals Logo" class="sponsor-logo">
			    	<p class="sponsor-title">Lady Anne Funerals - West Ryde</p>	
			    </a>
			    <!-- <a href="http://www.sascodevelopments.com.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/SascoLogo.png" alt="Sasco Developments"  class="sponsor-logo">
			    	<p class="sponsor-title">Sasco Developments</p>
		      	</a>	 -->
		      	<!-- <a href="http://www.victordominello.com.au" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/victordominello.png" alt="Victor Dominello"  class="sponsor-logo">
				    <p class="sponsor-title">Victor Dominello</p>	
			    </a>
			    <a href="http://johnalexander.net.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/johnalexander.png" alt="John Alexander" class="sponsor-logo">
			    	<p class="sponsor-title">John Alexander</p>	
			    </a>
				<a href="http://ladyannefunerals.com.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/Lady_Anne_Funerals.jpg" alt="Lady Anne Funerals Logo" class="sponsor-logo">
			    	<p class="sponsor-title">Lady Anne Funerals - West Ryde</p>	
			    </a>
			    <a href="http://agents.helloworld.com.au/helloworld-top-ryde-city" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/helloworld.jpg"  alt="Hello World"  class="sponsor-logo">
			    	<p class="sponsor-title">Hello World - Top Ryde</p>	
			    </a> -->
	      	</div> 	
	      	<hr>
	    </section>
	    
		<section id="sponsors-community-plus" class="container sponsors">
	    	<div class="row">
		    	<div class="col-sm-10 text-center">
		      		<h2 class="large color-yellow">Community + Sponsors</h2>
		      	</div>
	      	</div>
	      	<div class="row row-eq-height text-center" style="">
		      	<a href="" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/midway.jpg"  alt="Midway Cellars"  class="sponsor-logo">
			    	<p class="sponsor-title">Midway Cellars - Sponsor of Leichhardt Celebrity Brass Band</p>	
			    </a>
			    
		      	<a href="http://www.electricsunshine.com.au" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/electricsunshine.jpg"  alt="Electric Sunshine logo"  class="sponsor-logo">
			    	<p class="sponsor-title">Electric Sunshine</p>	
			    </a>
			    <a href="http://www.eastwoodhotel.com.au" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/eastwood_logo.jpg" alt="Eastwood Hotel"  class="sponsor-logo">
			    	<p class="sponsor-title">Eastwood Hotel</p>	
			    </a>

			    <a href="http://agents.helloworld.com.au/helloworld-top-ryde-city" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/helloworld.jpg"  alt="Hello World"  class="sponsor-logo">
			    	<p class="sponsor-title">Hello World - Top Ryde</p>	
			    </a> 
			    
			    <a href="https://www.theranch.com.au/" target="_blank" class="col-sm-2 col-xs-4  sponsor-block">
				    <img src="images/sponsors/ranch.png"  alt="The Ranch Hotel"  class="sponsor-logo">
			    	<p class="sponsor-title">The Ranch Hotel</p>	
			    </a>
			    <a href="http://www.yellowpages.com.au/nsw/east-ryde/group-travel-management-12094866-listing.html" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <p class="sponsor-logo">Bunnings</p>
			    	<p class="sponsor-title">Carlingford</p>	
			    </a>

			    

	      	</div>	      	
	      	<hr>
	    </section>
	    
	    
	    <section id="sponsors-community" class="container sponsors">
	      	<div class="row">
		    	<div class="col-sm-10 text-center">
					<h2 class="large color-orange">Community Sponsors</h2>
		      	</div>
	      	</div>
			<div class="row row-eq-height text-center" style="">
				 <a href="http://www.allegianceloans.com.au/" target="_blank" class="col-sm-2 col-sm-offset-1  col-xs-4 sponsor-block">
				    <img src="images/sponsors/Allegiance_Logo.jpg"  alt="Allegiance Home Loans"  class="sponsor-logo">
			    	<p class="sponsor-title">Allegiance Home Loans</p>	
			    </a>
			    <!-- <a href="" target="_blank" class="col-sm-3  col-xs-6 sponsor-block">
					<p class="sponsor-logo" >Combined Chambers of Commerce</p>
			    	<p class="sponsor-title">Riverside Business Chamber, Eastwood Chamber of Commerce</p>	
			    </a> -->
			    <a href="http://www.rydetoyota.com.au" target="_blank" class="col-sm-3 col-xs-6 sponsor-block">
				    <img src="images/sponsors/toyota.jpg"  alt="Ryde Toyota"  class="sponsor-logo">
			    	<p class="sponsor-title">Ryde Toyota</p>	
			    </a>
			    <a href="http://www.yellowpages.com.au/nsw/east-ryde/group-travel-management-12094866-listing.html" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <p class="sponsor-logo">Group Travel Management</p>
			    	<p class="sponsor-title">Group Travel Management</p>	
			    </a>

<!--
			    <a href="http://agents.helloworld.com.au/helloworld-top-ryde-city" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/helloworld.jpg"  alt="Hello World"  class="sponsor-logo">
			    	<p class="sponsor-title">Hello World - Top Ryde</p>	
			    </a>
-->
<!--
			    <a href="http://2rrr.org.au/" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/2rrr.jpg"  alt="2RRR Community Radio Station Logo" class="sponsor-logo">
			    	<p class="sponsor-title">2RRR Radio</p>	
			    </a> 
-->
			    <!--
			    <a href="http://www.nextgenclubs.com.au/home/ryde.aspx" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/nextgen.png"  alt="Next Gen Ryde"  class="sponsor-logo" style="background-color:#1C2B39; padding:3px;">
			    	<p class="sponsor-title">Next Gen Ryde</p>	
			    </a>
			    <a href="http://www.ritchies.com.au/location/north-ryde" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/IGA_logo.jpg"  alt="Ritchies IGA Cox's Road"  class="sponsor-logo">
			    	<p class="sponsor-title">Ritchies IGA Cox's Road</p>	
			    </a>
			    
			    <a href="http://STARStv.com.au" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <img src="images/sponsors/starstv12.jpg"  alt="Stars TV" class="sponsor-logo">
			    	<p class="sponsor-title">Stars TV</p>	
			    </a>
			    
			    
			    <a href="http://coxsroadmall.com.au/stores/fruit-market" target="_blank" class="col-sm-2 col-xs-4 sponsor-block">
				    <p class="sponsor-logo">Cox's Rd Fruit Market</p>
			    	<p class="sponsor-title">Cox's Rd Fruit Market</p>	
			    </a> -->		    
	      	</div>	      	
	      	<hr>
	    </section>
    </div>

    <?php include_once("inc/footer.php"); ?></body>
</html>