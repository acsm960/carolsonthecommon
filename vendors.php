<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Vendor application, Carols on the Common event in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 18th Dec 2016"/>
    <meta property="og:description" content="Vendor application, Carols on the Common event in North Ryde."/>
    
    <title>Vendors | Christmas Carols in North Ryde | 18th Dec 2016</title>
    
	<?php include_once("inc/head.php"); ?>
    </head><!--/head-->

<body class="vendors page">
	<?php include_once("inc/header.php"); ?>

	<div class="main-container xmas-lights" role="main">
	
		<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					<h1 class="large mb16 mb-xs-24">Stall Holder</h1>
						<p class="heading-line heading-line-lime super-lead">Application</p>
						<!-- <p class="lead"></p> -->
				</div>
			</div>
		</div>
	</section>
		
		<section class="promo-block promo-block-red">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-md-push-7">
						<h2>Instructions</h2>
						<p>Please fill in the form on the left.</p>
						<blockquote>
                <p>Vendor applications are processed by the committee on a first come basis.</p>
                <p>Unfortunately we can not accept all applications.  We reserve the right to select applications based on the product mix or any other factor  based on the objectives of the commitee.</p>
						</blockquote>
					</div>
					<div class="col-md-7 col-md-pull-5">
						<? if (file_exists('../lib')): ?>
								<? if (isset($_GET['thanks'])): ?>
									<? include 'inc/vendors_thanks.php' ?>
								<? else: ?>
									<? include 'inc/vendors_form.php' ?>
								<? endif ?>
						<? else: ?>
								<p>(Vendors form goes here)</p>
						<? endif ?>
					</div>
				</div>
			</div>
		</section>
		<hr> 
	</div>
	<?php include_once("inc/footer.php"); ?>
</body>
</html>
