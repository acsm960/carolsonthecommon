<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Voluteer to help at this year's Carols on the Common event in North Ryde.">
    
    <meta property="og:title" content="Rotary Carols on the Common | 15th Dec 2019"/>
    <meta property="og:description" content="Voluteer to help at this year's Carols on the Common event in North Ryde, 15th Dec 2019."/>
    
    <title>Volunteers | Christmas Carols in North Ryde | 15th Dec 2019</title>
    
	<?php include_once("inc/head.php"); ?>
    </head><!--/head-->

<body class="volunteers page">
	<?php include_once("inc/header.php"); ?>

	<div class="main-container xmas-lights" role="main">
	
		<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					<h1 class="large mb16 mb-xs-24">Volunteers</h1>
						<p class="heading-line heading-line-lime super-lead">Thanks for volunteering to help at this year's carols.</p>
						<!-- <p class="lead"></p> -->
				</div>
			</div>
		</div>
	</section>
		
		<section class="promo-block promo-block-red">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-md-push-7">
						<h2>Instructions</h2>
						<p>Thank you for volunteering to help out with this year's Carols on the Common.  Please assist by filling in the form on the left to help us schedule you appropriately.</p>
						<h3>Important - If you can't come:</h3>
						<p>If you register and can't make we don't mind but we really want to know.  
<script type="text/javascript">document.write('Please contact Paul Williamson on 02 9868 2595 (or 0425 859 876)');</script> or re-fill the form with your apology in the the comments.</p>
            <h3>FYI: No BBQ</h3>
            <p>Some of you may have previously enjoyed helping out with the BBQ.   Rotary has decided to replace the BBQ with vendors that see similar product.  This is because the expense of holding the BBQ together with all the additional origanisation required did not make it a profitable fund raising action.   We are sorry if this disappoints you.</p>
						<p>Here a rough schedule of what we will be doing on the day:</p>
						<blockquote>
              
						<h4>Day</h4>
						<ul>
							<li>Set up behind stage area (heavy work)</li>
						    <li>Set up audience area</li>
						    <li>Set up necessary fencing and stalls</li>
						</ul>
						<h4>Evening</h4>
						<ul>
              <li>Glow stick, Santa hats, etc stall</li>
              <li>Stage managers</li>
              <li>Waste Facilitation - this year council is not providing services to keep the ground clear.   We want to do all we can during the event to keep the place tidy with the emphasis on people being responsible for their own waste.</li>
              <li>Lost Elves Tent</li>
              <li>Pull down (BBQ, Stage, VIP Tent)</li>
						</ul>
						</blockquote>
						<h3>Bucket Collection</h3>
						<p>At about 7:30 pm we will be running a bucket collection to raise money for ROMAC from people who are around in the evening.</p>
						<!--
						<h3>Marketing Data Collection</h3>
						<p>We need people to interview attendees so we can use the information to improve the Carols from one year to the next.   The process involves interviewing attendees and entering their answer into a survey program (via any Apple or Android device).</p>
						-->
					</div>
					<div class="col-md-7 col-md-pull-5">
						<? if (file_exists('../lib')): ?>
								<? if (isset($_GET['thanks'])): ?>
									<? include 'inc/volunteers_thanks.php' ?>
								<? else: ?>
									<? include 'inc/volunteers_form.php' ?>
								<? endif ?>
						<? else: ?>
								<p>(Volunteers form goes here)</p>
						<? endif ?>
					</div>
				</div>
			</div>
		</section>
		<hr> 
	</div>
	<?php include_once("inc/footer.php"); ?>
</body>
</html>
